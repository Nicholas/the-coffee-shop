/*
 * FactoryErrorPolicies.hpp
 *
 *  Created on: 5 Nov 2013
 *      Author: Nicholas Welters
 */

# ifndef FACTORYERRORPOLICIES_HPP_
	# define FACTORYERRORPOLICIES_HPP_

	# include "../Common/Includes.hpp"

	namespace Tools
	{
		template< class IdentifierType, class AbstractProduct >
		class DefaultFactoryError
		{

			public:
				class Exception: public std::exception
				{
					private:
						IdentifierType unknownID;

					public:
						Exception( const IdentifierType & unknownID_ )
							: unknownID( unknownID_ )
						{ }

						virtual const char * what( )
						{
							return "Unknown Identifier Type";
						}

						const IdentifierType & getIdentifier( )
						{
							return unknownID;
						}
				};

			protected:
				static AbstractProduct * OnUnknownError( const IdentifierType & id )
				{
					throw Exception( id );
				}
		};

	}
# endif /* FACTORYERRORPOLICIES_HPP_ */
