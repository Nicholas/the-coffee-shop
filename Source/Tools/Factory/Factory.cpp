/*
 * Factory.cpp
 *
 *  Created on: 5 Nov 2013
 *      Author: Nicholas Welters
 */


# ifndef FACTORY_CPP_
	# define FACTORY_CPP_

	# include "Factory.hpp"

	namespace Tools
	{
		template
		<
			class AbstractProduct,
			class IdentifierType,
			class ProductCreator,
			template< typename, class >
			class FactoryErrorPolicy
		>
		Factory< AbstractProduct, IdentifierType, ProductCreator, FactoryErrorPolicy >::Factory( )
			: FactoryErrorPolicy< IdentifierType, AbstractProduct >( )
		{ }

		template
			<
				class AbstractProduct,
				class IdentifierType,
				class ProductCreator,
				template< typename, class >
				class FactoryErrorPolicy
			>
		bool Factory< AbstractProduct, IdentifierType, ProductCreator, FactoryErrorPolicy >::registerProduct( const IdentifierType & id, ProductCreator creator )
		{
			return workers.insert( typename Workers::value_type( id, creator ) ).second;
		}

		template
			<
				class AbstractProduct,
				class IdentifierType,
				class ProductCreator,
				template< typename, class >
				class FactoryErrorPolicy
			>
		bool Factory< AbstractProduct, IdentifierType, ProductCreator, FactoryErrorPolicy >::unregisterProduct( const IdentifierType & id )
		{
			return workers.erase( id ) == 1;
		}

		template
			<
				class AbstractProduct,
				class IdentifierType,
				class ProductCreator,
				template< typename, class >
				class FactoryErrorPolicy
			>
		AbstractProduct * Factory< AbstractProduct, IdentifierType, ProductCreator, FactoryErrorPolicy >::createProduct( const IdentifierType & id )
		{
			typename Workers::iterator i = workers.find( id );

			if( i != workers.end( ) )
			{
				return i->second.operator( )( );
			}

			return FactoryErrorPolicy< IdentifierType, AbstractProduct >::OnUnknownError( id );
		}
	} /* namespace Tools */
# endif /* FACTORY_HPP_ */
