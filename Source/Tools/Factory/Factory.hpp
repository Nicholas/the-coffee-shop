/*
 * Factory.hpp
 *
 *  Created on: 5 Nov 2013
 *      Author: Nicholas Welters
 */

# ifndef FACTORY_HPP_
	# define FACTORY_HPP_

	# include "FactoryErrorPolicies.hpp"
	# include "../Common/Includes.hpp"

	namespace Tools
	{
		template
		<
			class AbstractProduct,
			class IdentifierType,
			class ProductCreator = AbstractProduct * ( * )( ),
			template< typename, class >
			class FactoryErrorPolicy = DefaultFactoryError
		>
		class Factory : public FactoryErrorPolicy< IdentifierType, AbstractProduct >
		{
			private:
				typedef map< IdentifierType, ProductCreator > Workers;
				Workers workers;

			public:
				Factory( );

				bool registerProduct( const IdentifierType & id, ProductCreator creator );
				bool unregisterProduct( const IdentifierType & id );
				AbstractProduct * createProduct( const IdentifierType & id );
		};
	} /* namespace Tools */

	# include "Factory.cpp"

# endif /* FACTORY_HPP_ */
