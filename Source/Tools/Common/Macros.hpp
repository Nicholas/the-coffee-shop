/*
 * Macros.hpp
 *
 *  Created on: 28 Oct 2013
 *      Author: Nicholas Welters
 */

#ifndef MACROS_HPP_
	#define MACROS_HPP_

	# define unused( x ) ( void )( x )

#endif /* MACROS_HPP_ */
