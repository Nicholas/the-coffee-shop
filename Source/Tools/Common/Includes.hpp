/*
 * Includes.hpp
 *
 *  Created on: 28 Oct 2013
 *      Author: Nicholas Welters
 */

# ifndef INCLUDES_HPP_
	# define INCLUDES_HPP_

	# define __GXX_EXPERIMENTAL_CXX11__

	// System Includes
	# include <cstdio>
	# include <cstdlib>
	# include <iostream>
	# include <cstring>
	# include <stdexcept>
	# include <sstream>
	# include <memory>
	# include <map>
	# include <utility>

	namespace Tools
	{
		using std::cout;
		using std::cerr;
		using std::endl;
		using std::string;
		using std::stringstream;
		using std::map;

		using std::exception;
		using std::logic_error;
		using std::out_of_range;

		using std::unique_ptr;

		using std::forward;
	}

# endif /* INCLUDES_HPP_ */
