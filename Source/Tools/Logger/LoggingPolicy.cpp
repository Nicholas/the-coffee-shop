/*
 * LogPolicy.cpp
 *
 *  Created on: 27 Apr 2013
 *      Author: Nicholas Welters
 */

# include "LoggingPolicy.hpp"

# ifndef _LOGGING_POLICY_CPP_
# define _LOGGING_POLICY_CPP_

namespace Tools
{
    template< typename AbstractLog >
    void CerrLogger< AbstractLog >::logPolicy( const AbstractLog & message )
    {
        cerr << message << "\n";
    }

    template< typename AbstractLog >
    void CoutLogger< AbstractLog >::logPolicy( const AbstractLog & message )
    {
        cout << message << "\n";
    }
} /* namespace GenX */

# endif /* _LOGGING_POLICY_CPP_ */

