/* 
 * File:   Logger.hpp
 * Author: Nicholas Welters
 *
 * Created on 21 April 2013, 16:28
 */
# include "LoggingPolicy.hpp"

# ifndef _LOGGER_HPP_
	# define _LOGGER_HPP_

	namespace Tools
	{
		template< typename AbstractLog, template< typename > class LoggingPolicy >
		class Log : public LoggingPolicy< AbstractLog >
		{
		  public:
			Log( );
			Log( const Log& original );
			virtual ~Log( );

			void log( const AbstractLog & message );

			void logf( char const * s );

			template< typename T, typename... Args >
			void logf( const char * s, T value, Args... args );
		};
	}/* namespace GenX */

	# include "Logger.cpp"

# endif	/* LOGGER_HPP */

