/*
 * LogPolicy.h
 *
 *  Created on: 27 Apr 2013
 *      Author: Nicholas Welters
 */

# include "../Common/Includes.hpp"

# ifndef _LOGGING_POLICY_HPP_
# define _LOGGING_POLICY_HPP_

namespace Tools
{
	template< typename AbstractLog >
	class CerrLogger
	{
		public:
			void logPolicy( const AbstractLog & message );

		protected:
			CerrLogger( ) { };
			~CerrLogger( ) { };
	};

	template< typename AbstractLog >
	class CoutLogger
	{
		public:
			void logPolicy( const AbstractLog & message );

		protected:
			CoutLogger( ) { };
			~CoutLogger( ) { };
	};
} /* namespace GenX */

# include "LoggingPolicy.cpp"

# endif /* _LOGGING_POLICY_HPP_ */
