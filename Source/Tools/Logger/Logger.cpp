/* 
 * File:   Logger.cpp
 * Author: Nicholas Welters
 * 
 * Created on 21 April 2013, 16:28
 */

# include "Logger.hpp"

# ifndef _LOGGER_CPP_
# define _LOGGER_CPP_

namespace Tools
{
    template< typename AbstractLog, template< typename > class LoggingPolicy >
    Log< AbstractLog, LoggingPolicy >::Log( )
    : LoggingPolicy< AbstractLog >( )
    { }

    template< typename AbstractLog, template< typename > class LoggingPolicy >
    Log< AbstractLog, LoggingPolicy >::Log( const Log & original )
    : LoggingPolicy< AbstractLog >( )
    {
        unused( original );
    }

    template< typename AbstractLog, template< typename > class LoggingPolicy >
    Log< AbstractLog, LoggingPolicy >::~Log( ) { }

    template< typename AbstractLog, template< typename > class LoggingPolicy >
    void Log< AbstractLog, LoggingPolicy >::log( const AbstractLog & message )
    {
    	LoggingPolicy< AbstractLog >::logPolicy( message );
    }

    template< typename AbstractLog, template< typename > class LoggingPolicy >
    void Log< AbstractLog, LoggingPolicy >::logf( const char * s )
    {
        while (*s)
        {
            cout << *s++;
        }

        cout << "\n";
    }
    
    template< typename AbstractLog, template< typename > class LoggingPolicy >
    template< typename T, typename... Args >
    void Log< AbstractLog, LoggingPolicy >::logf( const char * s, T value, Args... args )
    {
        while (*s)
        {
            if (*s == '%')
            {
                if (*(s + 1) == '%')
                {
                    ++s;
                }
                else
                {
                    cout << value;
                    logf(s + 1, args...); // call even when *s == 0 to detect extra arguments
                    return;
                }
            }
            cout << *s++;
        }
        throw logic_error("Extra arguments provided to logf");
    }

}/* namespace GenX */

# endif	/* LOGGER_CPP */
