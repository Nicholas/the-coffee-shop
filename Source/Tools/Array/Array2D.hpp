/*
 *  Array2D.hpp
 *
 *  Created on: 02 July 2012
 *      Author: Nicholas Welters
 *
 *		Two Dimensional Array Template class.
 *      Array template class for memory protection
 *      so i don't have to reinstall windows again!
 *
 *      Has some general list methods.
 */
# ifndef _ARRAY_2D_HPP_
	# define _ARRAY_2D_HPP_

	# include "Array1D.hpp"

	namespace Tools
	{
		template< typename CLASS >
		class Array2D
		{
			private:
				//////////////
				// Members //
				////////////
				CLASS * * p_p_array; // Pointer to an array of the type specified.
				Array1D< Array1D< CLASS > * > a_p_arrayProtection; // Array maintenance.

				//////////////
				// Methods //
				////////////
				// -------------------------------------------
				// Default Constructor
				// -------------------------------------------
				Array2D( void );

			public:
				//////////////
				// Methods //
				////////////
				// -------------------------------------------
				// Constructors
				// -------------------------------------------
				Array2D( Array2D< CLASS > & copy );
				Array2D( size_t length_0, size_t length_1 );
				Array2D( CLASS * * p_array, size_t length_0, size_t length_1 );

				// -------------------------------------------
				// Destructor
				// -------------------------------------------
				~Array2D( void );

				// -------------------------------------------
				// Operators
				// -------------------------------------------
				Array2D< CLASS > & operator =( Array2D< CLASS > & copy );

				Array1D< CLASS > & operator []( size_t index );

				// -------------------------------------------
				// Getters & Setters
				// -------------------------------------------
				size_t size( );
				CLASS * * pointer( );

				Array1D< CLASS > & get( size_t index );
				CLASS & get( size_t index_0, size_t index_1 );
				void set( size_t index_0, size_t index_1, CLASS & data );
		};

	} /* namespace Tools */

	# include "Array2D.cpp"

# endif
