/* 
 * File:   Array.hpp
 * Author: Nicholas Welters
 *
 * Created on 22 April 2013, 18:32
 */

// TODO: Create an ownership policy. This will define if this call is responsible to deleting the array.
// 			--OwnArray
//			--LoneArray: This is holing an array that it must not delete.
//						 This must throw exceptions all constructors except the wrapper constructor.
//						 This is to enforce the client to have known about its array before unknowingly created one that he must now delete.

# ifndef ARRAY_HPP
	# define ARRAY_HPP

	# include "Array1D.hpp"

	namespace Tools
	{
		// Array Alias
		template< typename CLASS >
		using Array = Array1D< CLASS >;
	}
# endif	/* ARRAY_HPP */

