/*
 *	Array1D.cpp
 *
 *  Created on: 02 July 2012
 *      Author: Nicholas Welters
 */
# include "Array1D.hpp"

# ifndef _ARRAY_1D_CPP_
	# define _ARRAY_1D_CPP_

	namespace Tools
	{
		// -------------------------------------------
		// Constructors
		// -------------------------------------------
		template< typename CLASS >
		Array1D< CLASS >::Array1D( size_t length_ )
				: p_array( nullptr ), length( length_ )
		{
			if( length > 0 )
			{
				p_array = new CLASS[ length ];
			}
		}

		template< typename CLASS >
		Array1D< CLASS >::Array1D( CLASS * p_array_, size_t length_ )
				: p_array( p_array_ ), length( length_ )
		{
		}

		template< typename CLASS >
		Array1D< CLASS >::Array1D( Array1D< CLASS > & copy )
				: p_array( nullptr ), length( copy.length )
		{
			p_array = new CLASS[ length ];

			for( size_t i = 0; i < length; ++i )
			{
				p_array[ i ] = copy.p_array[ i ];
			}
		}

		// -------------------------------------------
		// Destructor
		// -------------------------------------------
		template< typename CLASS >
		Array1D< CLASS >::~Array1D( void )
		{
			if( p_array )
			{
				delete[ ] p_array;
			}
		}

		// -------------------------------------------
		// Operators
		// -------------------------------------------
		template< typename CLASS >
		Array1D< CLASS > & Array1D< CLASS >::operator =( Array1D< CLASS > & copy )
		{
			if( this != &copy )
			{
				if( length != copy.length )
				{
					if( p_array )
					{
						delete[ ] p_array;
					}

					length = copy.length;
					p_array = new CLASS[ length ];
				}

				for( size_t i = 0; i < length; ++i )
				{
					p_array[ i ] = copy.p_array[ i ];
				}
			}

			return ( *this );
		}

		template< typename CLASS >
		CLASS & Array1D< CLASS >::operator []( size_t index )
		{
			return ( p_array[ checkIndex( index ) ] );
		}

		// -------------------------------------------
		// Getters & Setters
		// -------------------------------------------
		template< typename CLASS >
		size_t Array1D< CLASS >::size( )
		{
			return ( length );
		}

		template< typename CLASS >
		CLASS * Array1D< CLASS >::pointer( )
		{
			return ( p_array );
		}

		template< typename CLASS >
		CLASS & Array1D< CLASS >::get( size_t index )
		{
			return ( p_array[ checkIndex( index ) ] );
		}

		template< typename CLASS >
		void Array1D< CLASS >::set( size_t index, CLASS & data )
		{
			p_array[ checkIndex( index ) ] = data;
		}

		// -------------------------------------------
		// Helpers
		// -------------------------------------------
		template< typename CLASS >
		size_t Array1D< CLASS >::checkIndex( size_t & index )
		{
			if( index >= length )
			{
				stringstream error;
				error << "Index out of bounds: Attempting to access index ";
				error << index;
				error << " in an array of length ";
				error << length;

				throw out_of_range( error.str( ) );
			}

			return ( index );
		}
	} /* namespace Tools */
# endif
