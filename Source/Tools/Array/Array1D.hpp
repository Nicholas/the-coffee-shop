/*
 *  Array1D.hpp
 *
 *  Created on: 02 July 2012
 *      Author: Nicholas Welters
 *
 *      Array template class for memory protection
 *      So I don't have to reinstall windows again!
 *
 *      Has some general list methods.
 */

# include <stddef.h>

# ifndef _ARRAY_1D_HPP_
	# define _ARRAY_1D_HPP_

	# include "../Common/Includes.hpp"

	namespace Tools
	{
		template< typename CLASS >
		class Array1D
		{
			private:
				//////////////
				// Members //
				////////////
				CLASS * p_array; // Pointer to an array of the type specified
				size_t length; // Size of the array of specified type

				//////////////
				// Methods //
				////////////
				// -------------------------------------------
				// Default Constructor
				// -------------------------------------------
				Array1D( void );

				// -------------------------------------------
				// Checks the index.
				// The value returned is a valid index.
				// -------------------------------------------
				size_t checkIndex( size_t & index );

			public:
				//////////////
				// Methods //
				////////////
				// -------------------------------------------
				// Constructors
				// -------------------------------------------
				Array1D( size_t length );
				Array1D( CLASS * p_array, size_t length );
				Array1D( Array1D< CLASS > & original );

				// -------------------------------------------
				// Destructor
				// -------------------------------------------
				~Array1D( void );

				// -------------------------------------------
				// Operators
				// -------------------------------------------
				Array1D< CLASS > & operator=( Array1D< CLASS > & copy );

				CLASS & operator[]( size_t index );

				// -------------------------------------------
				// Getters & Setters
				// -------------------------------------------
				size_t size( );
				CLASS * pointer( );

				CLASS & get( size_t index );
				void set( size_t index, CLASS & data );
		};
	} /* namespace Tools */

	# include "Array1D.cpp"
# endif
