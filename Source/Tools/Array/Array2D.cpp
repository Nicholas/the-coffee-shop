/*
 *	Array2D.cpp
 *
 *  Created on: 02 July 2012
 *      Author: Nicholas Welters
 */
# include "Array2D.hpp"

# ifndef _ARRAY_2_CPP_
	# define _ARRAY_2_CPP_

	namespace Tools
	{
		// -------------------------------------------
		// Constructors
		// -------------------------------------------
		template< typename CLASS >
		Array2D< CLASS >::Array2D( size_t length_0, size_t length_1 )
				: p_p_array( nullptr )
				, a_p_arrayProtection( length_0 )
		{
			p_p_array = new CLASS *[ length_0 ];

			for( size_t i = 0; i < length_0; ++i )
			{
				p_p_array[ i ] = new CLASS[ length_1 ];
				a_p_arrayProtection[ i ] = new Array1D< CLASS >( p_p_array[ i ], length_1 );
			}
		}

		template< typename CLASS >
		Array2D< CLASS >::Array2D( CLASS * * p_array, size_t length_0, size_t length_1 )
				: p_p_array( p_array )
				, a_p_arrayProtection( length_0 )
		{
			for( size_t i = 0; i < length_0; ++i )
			{
				a_p_arrayProtection[ i ] = new Array1D< CLASS >( p_p_array[ i ], length_1 );
			}
		}

		template< typename CLASS >
		Array2D< CLASS >::Array2D( Array2D< CLASS > & copy )
				: p_p_array( nullptr )
				, a_p_arrayProtection( 0 )
		{
			operator = ( copy );
		}

		// -------------------------------------------
		// Destructor
		// -------------------------------------------
		template< typename CLASS >
		Array2D< CLASS >::~Array2D( void )
		{
			for( size_t i = 0; i < a_p_arrayProtection.size( ); ++i )
			{
				if( a_p_arrayProtection[ i ] )
				{
					delete a_p_arrayProtection[ i ];
				}
			}

			if( p_p_array )
			{
				delete[ ] p_p_array;
			}
		}

		// -------------------------------------------
		// Operators
		// -------------------------------------------
		template< typename CLASS >
		Array2D< CLASS > & Array2D< CLASS >::operator =( Array2D< CLASS > & copy )
		{
			if( this != &copy )
			{
				if( a_p_arrayProtection.size( ) != copy.a_p_arrayProtection.size( ) )
				{
					for( size_t i = 0; i < a_p_arrayProtection.size( ); ++i )
					{
						if( a_p_arrayProtection[ i ] )
						{
							delete a_p_arrayProtection[ i ];
						}
					}

					if( p_p_array )
					{
						delete[ ] p_p_array;
					}

					Array1D< Array1D< CLASS > * > tempArray( copy.a_p_arrayProtection.size( ) );

					a_p_arrayProtection = tempArray;
					p_p_array = new CLASS *[ a_p_arrayProtection.size( ) ];

					for( size_t i = 0; i < a_p_arrayProtection.size( ); ++i )
					{
						a_p_arrayProtection[ i ] = new Array1D< CLASS >( ( * copy.a_p_arrayProtection[ i ] ) );
						p_p_array[ i ] = a_p_arrayProtection[ i ]->pointer( );
					}
				}
				else
				{
					for( size_t i = 0; i < a_p_arrayProtection.size( ); ++i )
					{
						(*a_p_arrayProtection[ i ]) = (*copy.a_p_arrayProtection[ i ]);
						p_p_array[ i ] = a_p_arrayProtection[ i ]->pointer( );
					}
				}
			}

			return *this;
		}

		template< typename CLASS >
		Array1D< CLASS > & Array2D< CLASS >::operator []( size_t index )
		{
			return (*a_p_arrayProtection[ index ]);
		}

		// -------------------------------------------
		// Getters & Setters
		// -------------------------------------------
		template< typename CLASS >
		size_t Array2D< CLASS >::size( )
		{
			return a_p_arrayProtection.size( );
		}

		template< typename CLASS >
		CLASS * * Array2D< CLASS >::pointer( )
		{
			return p_p_array;
		}

		template< typename CLASS >
		Array1D< CLASS > & Array2D< CLASS >::get( size_t index )
		{
			return (*this)[ index ];
		}

		template< typename CLASS >
		CLASS & Array2D< CLASS >::get( size_t index_0, size_t index_1 )
		{
			return (*a_p_arrayProtection[ index_0 ])[ index_1 ];
		}

		template< typename CLASS >
		void Array2D< CLASS >::set( size_t index_0, size_t index_1, CLASS & data )
		{
			a_p_arrayProtection[ index_0 ]->set( index_1, data );
		}
	} /* namespace Tools */
# endif
