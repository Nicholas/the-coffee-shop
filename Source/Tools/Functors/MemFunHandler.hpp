/*
 * MemFunHandler.hpp
 *
 *  Created on: 3 Nov 2013
 *      Author: nic
 */

# ifndef MEMFUNHANDLER_HPP_
	# define MEMFUNHANDLER_HPP_

	# include "../Common/Includes.hpp"
	# include "FunctorImpl.hpp"

	namespace Tools
	{

		template< class ParentFunctor, typename PointerToObj, typename PointerToMemFn, typename... Args >
		class MemFunHandler : public FunctorImpl
		<
			typename ParentFunctor::Return,
			Args...
		>
		{
			private:
				PointerToObj p_obj;
				PointerToMemFn p_memFn;

			public:
				typedef typename ParentFunctor::Return Return;

				MemFunHandler( const PointerToObj & p_obj_, PointerToMemFn p_memFn_ )
				: p_obj( p_obj_ ), p_memFn( p_memFn_ ) { }

				MemFunHandler * clone( ) const
				{
					return new MemFunHandler( * this );
				}

				Return operator( )( Args&&... args )
				{
					return ( ( * p_obj ).*p_memFn )( forward< Args >( args )... );
				}
		};

	} /* namespace Tools */
#endif /* MEMFUNHANDLER_HPP_ */
