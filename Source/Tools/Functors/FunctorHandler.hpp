/*
 * FunctorHandler.hpp
 *
 *  Created on: 3 Nov 2013
 *      Author: Nicholas Welters
 */

# ifndef FUNCTORHANDLER_HPP_
	# define FUNCTORHANDLER_HPP_

	# include "../Common/Includes.hpp"
	# include "FunctorImpl.hpp"

	namespace Tools
	{
		template< class ParentFunctor, typename FunctorType, typename... Args >
		class FunctorHandler : public FunctorImpl
		<
			typename ParentFunctor::Return,
			Args...
		>
		{
			private:
				FunctorType functor;

			public:
				typedef typename ParentFunctor::Return Return;

				FunctorHandler( const FunctorType & functor_ )
					: functor( functor_ ){ }

				FunctorHandler * clone( ) const
				{
					return new FunctorHandler( * this );
				}

				Return operator( )( Args&&... args )
				{
					return ( functor( forward< Args >( args )... ) );
				}
		};

	} /* namespace Tools */
# endif /* FUNCTORHANDLER_HPP_ */
