/*
 * FunctorImpl.hpp
 *
 *  Created on: 3 Nov 2013
 *      Author: Nicholas Welters
 */

# ifndef FUNCTORIMPL_HPP_
	# define FUNCTORIMPL_HPP_

	namespace Tools
	{
		//////////////////////////////////
		// Functor Implementation Type //
		////////////////////////////////
		template< class ReturnType, typename... Args >
		class FunctorImpl {
			public:
				virtual ReturnType operator( )( Args&&... ) = 0;
				virtual FunctorImpl * clone( ) const = 0;
				virtual ~FunctorImpl( ) { }
		};
	} /* namespace Tools */
# endif /* FUNCTORIMPL_HPP_ */
