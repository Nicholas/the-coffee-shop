/*
 * Functor.hpp
 *
 *  Created on: 3 Nov 2013
 *      Author: Nicholas Welters
 */

#ifndef FUNCTOR_HPP_
	# define FUNCTOR_HPP_

	# include "../Common/Includes.hpp"
	# include "FunctorImpl.hpp"
	# include "FunctorHandler.hpp"
	# include "MemFunHandler.hpp"

	namespace Tools
	{
		template< class R, typename... Args >
		class Functor;
	}

	# include "Binding.hpp"

	namespace Tools
	{
		template< class R, typename... Args >
		class Functor
		{
			public:
				typedef R Return;

			private:
				typedef FunctorImpl< Return, Args... > Impl;
				unique_ptr< Impl > up_impl;

			public:
				Functor( );
				Functor & operator=( const Functor& );
				explicit Functor( unique_ptr< Impl > up_impl );

				template< class FunctorType >
				Functor( const FunctorType & functor )
				: up_impl( new FunctorHandler< Functor, FunctorType, Args... >( functor ) )
				{ }

				template< class PrtObj, class MemFn >
				Functor( const PrtObj & p, const MemFn & memFn )
				: up_impl( new MemFunHandler< Functor, PrtObj, MemFn, Args... >( p, memFn ) )
				{ }


				Functor( const Functor & original )
				: up_impl( original.up_impl->clone( ) )
				{

				}

				virtual ~Functor( ) { };

				Return operator( )( Args&&... args )
				{
					return ( ( * up_impl )( forward< Args >( args )... ) );
				}
		};
	}

	# include "Functor.cpp"

# endif /* FUNCTOR_HPP_ */
