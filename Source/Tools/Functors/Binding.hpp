/*
 * Binding.hpp
 *
 *  Created on: 3 Nov 2013
 *      Author: Nicholas Welters
 */

# ifndef BINDING_HPP_
	# define BINDING_HPP_

	# include "FunctorImpl.hpp"

	namespace Tools
	{
		template< class Incoming, class Bound, typename... Args >
		class BinderFirst : public FunctorImpl
		<
			typename Incoming::Return,
			Args...
		>
		{
			public:
				typedef Functor< typename Incoming::Return, Args... > Outgoing;
				typedef typename Incoming::Return Return;

			private:
				Incoming function;
				Bound bound;

			public:
				BinderFirst( const Incoming & function_, Bound bound_ )
					: function( function_ ),
					  bound( bound_ )
				{
				}

				BinderFirst * clone( ) const
				{
					return new BinderFirst( *this );
				}

				Return operator( )( Args&&... args )
				{
					return function( bound, std::forward< Args >( args )... );
				}
		};


		/*
		template < class Fctor, class FirstArg >
		typename Private::BinderFirstTraits<Fctor>::BoundFunctionType
		BindFirst( const Fctor & functor, FirstArg bound )
		{
				typedef typename Private::BinderFirstTrait
		}
		*/

	} /* namespace Tools */
# endif /* BINDING_HPP_ */
