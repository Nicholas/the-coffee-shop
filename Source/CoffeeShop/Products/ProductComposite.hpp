/**
 *  A Little Coffee Shop
 *  ____________________
 *
 *  This is the composite implementation of the
 *  Product class. This implements the important
 *  add, remove and get methods.
 *
 *  @ Project : CoffeeShop
 *  @ File Name : ProductComposite.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


# ifndef _PRODUCTCOMPOSITE_H
	# define _PRODUCTCOMPOSITE_H

	# include "Product.hpp"
	# include "../../Configuration.hpp"

	namespace CoffeeShop
	{
		namespace Products
		{
			class ProductComposite : public Product
			{
				private:
					/**
					 * This is the total value of the composite.
					 */
					double totalValue;

					/**
					 * This is the container for the products that make up
					 * the composite.
					 */
					vector< ProductSP > products;

				public:
					ProductComposite( const string &, const double );
					virtual ~ProductComposite( );

					////////////////////////
					// Getters & Setters //
					//////////////////////
					virtual double getValue( ) const;

					///////////////////////
					// Prototype Method //
					/////////////////////
					virtual ProductComposite * clone( ) const;

					////////////////////////
					// Composite Methods //
					//////////////////////
					virtual void add( ProductSP );
					virtual void remove( ProductSP );
					virtual ProductSP get( int index ) const;
					virtual ProductSP get( size_t index ) const;
					virtual size_t size( ) const;
			};
		}
	}

# endif  //_PRODUCTCOMPOSITE_H
