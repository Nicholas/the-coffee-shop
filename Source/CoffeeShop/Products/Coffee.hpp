/**
 *  A Little Coffee Shop
 *  ____________________
 *
 *  @ Project : CoffeeShop
 *  @ File Name : Coffee.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


# ifndef _COFFEE_H
	# define _COFFEE_H

	# include "ProductComposite.hpp"

	namespace CoffeeShop
	{
		namespace Products
		{
			class Coffee;
			typedef shared_ptr< Coffee > CoffeeSP;

			class Coffee : public ProductComposite
			{
				public:
					Coffee( const string &, const double );
					virtual ~Coffee( );

					///////////////////////
					// Prototype Method //
					/////////////////////
					virtual Coffee * clone( ) const;
			};
		}
	}

# endif  //_COFFEE_H
