/**
 *  A Little Coffee Shop
 *  ____________________
 *
 *  This class forces a type strict
 *  implementation of the coffee
 *  shop rules.
 *
 *  @ Project : CoffeeShop
 *  @ File Name : Condement.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


# ifndef _CONDIMENT_H
	# define _CONDIMENT_H

	# include "Product.hpp"

	namespace CoffeeShop
	{
		namespace Products
		{
			class Condiment;
			typedef std::shared_ptr< Condiment > CondementSP;

			class Condiment : public Product
			{
				public:
					Condiment( const string &, const double );
					virtual ~Condiment( );

					///////////////////////
					// Prototype Method //
					/////////////////////
					virtual Condiment * clone() const;
			};
		}
	}

# endif  //_CONDIMENT_H
