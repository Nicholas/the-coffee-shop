/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : Product.cpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#include "Product.hpp"

namespace CoffeeShop
{
	namespace Products
	{
		Product::Product( const string & name_, const double value_ )
		: name ( name_  ),
		  value( value_ )
		{

		}
		
		Product::~Product( )
		{

		}
		
		string Product::getName( ) const
		{
			return name;
		}
		
		double Product::getValue( ) const
		{
			return value;
		}
		
		Product * Product::clone( ) const
		{
			return new Product( name, value );
		}
		
		void Product::add( ProductSP sp_product )
		{
			UNUSED( sp_product );
			throw logic_error( "Invalid Method Call to Product::add" );
		}
		
		void Product::remove( ProductSP sp_product )
		{
			UNUSED( sp_product );
			throw logic_error( "Invalid Method Call to Product::remove" );
		}
		
		ProductSP Product::get( int index ) const
		{
			UNUSED( index );
			throw logic_error( "Invalid Method Call to Product::getChild" );
		}
		
		ProductSP Product::get( size_t index ) const
		{
			UNUSED( index );
			throw logic_error( "Invalid Method Call to Product::getChild" );
		}

		size_t Product::size( ) const
		{
			return 0;
		}
	}
}
