/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : Condement.cpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


# include "Condiment.hpp"

namespace CoffeeShop
{
	namespace Products
	{
		Condiment::Condiment( const string & name_, const double value_ )
		: Product( name_, value_ )
		{

		}

		Condiment::~Condiment( )
		{

		}

		Condiment * Condiment::clone( ) const
		{
			return new Condiment( getName( ), getValue( ) );
		}
	}
}
