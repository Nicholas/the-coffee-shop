/**
 *  A Little Coffee Shop
 *  ____________________
 *
 *  This is the parent class for all the coffee shop products.
 *  This holds the products name and the value of that product.
 *
 *  As products can be made from a combination of products
 *  this class defines a composite interface. The default
 *  implementation will throw an Invalid_Method_Call_Excaption.
 *
 *  Not all useful composite methods are defined as they wont be used
 *  at this time.
 *
 *  This class also defines a clone method to take part
 *  in a prototype factory. This is used so that the server can
 *  define the name and price of products once and then allow
 *  the till to display the value to customers/assistants and also
 *  audit all the purchases in a consistent manner.
 *
 *  @ Project : CoffeeShop
 *  @ File Name : Product.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */

# ifndef _PRODUCT_H
	# define _PRODUCT_H

	# include "../../Configuration.hpp"

	namespace CoffeeShop
	{
		namespace Products
		{
			class Product;
			typedef std::shared_ptr< Product > ProductSP;

			class Product
			{
				private:
					/**
					 * The product name.
					 */
					string name;
					/**
					 * The products value in monetary terms.
					 */
					double value;

				public:
					Product( const string &, const double );
					virtual ~Product( );

					////////////////////////
					// Getters & Setters //
					//////////////////////
					/**
					 * @return The products name.
					 */
					virtual string getName( ) const;

					/**
					 * @return The products value.
					 */
					virtual double getValue( ) const;

					///////////////////////
					// Prototype Method //
					/////////////////////
					/**
					 * This is a deep copy construction method.
					 * We are returning a pointer to a Product rather than
					 * the shared pointer so that we can make use of
					 * covariant return types.
					 *
					 * @return An exact copy of this but in a new memory location ( Deep Copy :) )
					 */
					virtual Product * clone( ) const;

					////////////////////////
					// Composite Methods //
					//////////////////////
					/**
					 * Add a product to the composite.
					 * @throws logic_error
					 */
					virtual void add( ProductSP );

					/**
					 * Remove a product to the composite.
					 * @throws logic_error
					 */
					virtual void remove( ProductSP );

					/**
					 * Get on of the products that is in the composite.
					 * @throws logic_error
					 */
					virtual ProductSP get( int index ) const;
					virtual ProductSP get( size_t index ) const;

					/**
					 * Get on of the products that is in the composite.
					 */
					virtual size_t size( ) const;
			};
		}
	}

#endif  //_PRODUCT_H
