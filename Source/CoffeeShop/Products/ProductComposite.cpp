/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : ProductComposite.cpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


# include "ProductComposite.hpp"

namespace CoffeeShop
{
	namespace Products
	{
		ProductComposite::ProductComposite( const string & name_, const double value_ )
		: Product( name_, value_ ),
		  totalValue( value_ ),
		  products( )
		{

		}
		
		ProductComposite::~ProductComposite( )
		{

		}

		double ProductComposite::getValue( ) const
		{
			return totalValue;
		}
		
		ProductComposite * ProductComposite::clone( ) const
		{
			ProductComposite * p_composite = new ProductComposite( getName( ), Product::getValue( ) );

			vector< ProductSP >::const_iterator iter = products.begin( );

			for( ; iter != products.end( ); iter++ )
			{
				p_composite->add( ProductSP( ( * iter )->clone( ) ) );
			}

			return p_composite;
		}

		void ProductComposite::add( ProductSP sp_product )
		{
			totalValue += sp_product->getValue( );
			products.emplace_back( sp_product );
		}
		
		void ProductComposite::remove( ProductSP sp_product )
		{
			vector< ProductSP >::iterator iterator;

			for( iterator = products.begin( ); iterator != products.end( ); iterator++ )
			{
				if( sp_product == ( * iterator ) )
				{
					totalValue -= sp_product->getValue( );
					products.erase( iterator );
					return;
				}
			}
		}
		
		ProductSP ProductComposite::get( int index ) const
		{
			return products[ index ];
		}

		ProductSP ProductComposite::get( size_t index ) const
		{
			return products[ index ];
		}

		size_t ProductComposite::size( ) const
		{
			return products.size( );
		}
	}
}
