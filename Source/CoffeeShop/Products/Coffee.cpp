/**
 *  A Little Coffee Shop
 *  ____________________
 *
 *  @ Project : CoffeeShop
 *  @ File Name : Coffee.cpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#include "Coffee.hpp"

namespace CoffeeShop
{
	namespace Products
	{
		Coffee::Coffee( const string & name_, const double value_ )
		: ProductComposite( name_, value_ )
		{
		
		}
		
		Coffee::~Coffee( )
		{
		
		}

		Coffee * Coffee::clone( ) const
		{
			Coffee * p_coffee = new Coffee( getName( ), Product::getValue( ) );

			for( size_t i = 0; i < size( ); i++ )
			{
				p_coffee->add( ProductSP( get( i )->clone( ) ) );
			}

			return p_coffee;
		}
	}
}
