/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : CoffeeMachine.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


# ifndef _COFFEEMACHINE_H
	# define _COFFEEMACHINE_H

	# include "../../Configuration.hpp"

	# include "../Products/Coffee.hpp"

	namespace CoffeeShop
	{
		namespace Creators
		{
			using CoffeeShop::Products::Product;
			using CoffeeShop::Products::ProductSP;
			using CoffeeShop::Products::Coffee;
			using CoffeeShop::Products::CoffeeSP;

			class CoffeeMachine
			{
				public:
					typedef Functor< CoffeeSP > CoffeePot;
					typedef shared_ptr< CoffeePot > CoffeePotSP;

					typedef Functor< Product * > CondimentFactory;
					typedef Factory< Product, string, CondimentFactory > CondimentStore;
					typedef shared_ptr< CondimentStore > CondimentStoreSP;

				private:
					CoffeeSP sp_coffee;

					CoffeePotSP sp_coffeePot;
					CondimentStoreSP sp_condimentStore;

				public:
					CoffeeMachine( const CoffeePotSP &, const CondimentStoreSP & );

					virtual ~CoffeeMachine( );

					void brewCoffee( );
					CoffeeSP getCoffee( );

					void addCondement( const string & );
					double getCurrentValue( );
			};
		}
	}

# endif  //_COFFEEMACHINE_H
