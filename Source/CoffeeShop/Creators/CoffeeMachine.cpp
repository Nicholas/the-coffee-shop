/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : CoffeeMachine.cpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


# include "CoffeeMachine.hpp"

namespace CoffeeShop
{
	namespace Creators
	{
		CoffeeMachine::CoffeeMachine( const CoffeePotSP & sp_coffeePot_, const CondimentStoreSP & sp_condimentStore_ )
		: sp_coffee( nullptr ),
		  sp_coffeePot( sp_coffeePot_ ),
		  sp_condimentStore( sp_condimentStore_ )
		{

		}

		CoffeeMachine::~CoffeeMachine( )
		{

		}

		void CoffeeMachine::brewCoffee( )
		{
			sp_coffee = sp_coffeePot->operator( )( );
		}

		CoffeeSP CoffeeMachine::getCoffee( )
		{
			return sp_coffee;
		}

		void CoffeeMachine::addCondement( const string & id )
		{
			if( sp_coffee )
			{
				sp_coffee->add( ProductSP( sp_condimentStore->createProduct( id ) ) );
			}
		}

		double CoffeeMachine::getCurrentValue( )
		{
			if( sp_coffee )
			{
				return sp_coffee->getValue( );
			}

			return 0.0;
		}
	}
}
