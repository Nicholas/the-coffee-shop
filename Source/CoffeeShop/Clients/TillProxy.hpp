/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : TillProxy.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_TILLPROXY_H)
#define _TILLPROXY_H

#include "Till.hpp"
#include "Server.hpp"
#include "CoffeeShopTill.hpp"

namespace CoffeeShop
{
	namespace Clients
	{
		class TillProxy : public Till
		{
		public:
			void TillProxy(TillID id, Server server);
			CoffeeShopTill *RealSubject;
			void run();
		};
	}
}

#endif  //_TILLPROXY_H
