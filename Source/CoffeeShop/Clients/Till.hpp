/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : Till.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_TILL_H)
#define _TILL_H

#include "Server.hpp"
#include "CoffeeMachine.hpp"

namespace CoffeeShop
{
	namespace Clients
	{
		template <class TillID> class Till
		{
		public:
			void Till(TillID id, Server server);
			void ~Till();
			virtual void run() = 0;
			CoffeeMachine *builder;
		protected:
			Server server;
			TillID id;
		};
	}
}

#endif  //_TILL_H
