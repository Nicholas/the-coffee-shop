/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : CoffeeShopTill.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_COFFEESHOPTILL_H)
#define _COFFEESHOPTILL_H

#include "Till.hpp"
#include "Server.hpp"
#include "Index.hpp"
#include "CoffeeSelection.hpp"
#include "CondementSelection.hpp"
#include "History.hpp"
#include "CLIDisplay.hpp"

namespace CoffeeShop
{
	namespace Clients
	{
		class CoffeeShopTill : public Till
		{
		public:
			void CoffeeShopTill(TillID id, Server server);
			void ~CoffeeShopTill();
			void run();
			void run();
		private:
			Index index;
			CoffeeSelection coffeeSelection;
			CondementSelection condementSelection;
			History history;
			CLIDisplay display;
		};
	}
}

#endif  //_COFFEESHOPTILL_H
