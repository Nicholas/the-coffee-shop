/**
 *  A Little Coffee Shop
 *  ____________________
 *
 *  @ Project : CoffeeShop
 *  @ File Name : Observer.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_OBSERVER_H)
#define _OBSERVER_H


namespace CoffeeShop
{
	namespace Display
	{
		class Observer
		{
			public:
				virtual ~Observer( ){ }
				virtual void update( ) = 0;
		};
	}
}

#endif  //_OBSERVER_H
