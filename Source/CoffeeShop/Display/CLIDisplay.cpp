/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : CLIDisplay.cpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#include "CLIDisplay.hpp"

namespace CoffeeShop
{
	namespace Display
	{
		CLIDisplay::CLIDisplay( const DisplayModelSP & sp_model_ )
		: running( false ),
		  sp_model( sp_model_ )
		{
		
		}
		
		CLIDisplay::~CLIDisplay( )
		{

		}

		void CLIDisplay::update( )
		{
		}

		void CLIDisplay::display( )
		{
			running = true;
			string userInput = "";

			while( running )
			{
				cout << sp_model->getText( ) << endl;
				cout << endl;
				cout << "|     |::|" << endl;

				Array< string > keys = sp_model->getKeys( );

				for( size_t i = 0; i < keys.size( ); i++ )
				{
					cout << "|> " << keys[ i ] << " <|::|> " << sp_model->getText( keys[ i ] ) << endl;
				}

				cout << "|     |::|" << endl;
				cout << endl;
				cout << "Please enter selection: ";

				getline( cin, userInput );

				while( ! sp_model->contains( userInput ) && running )
				{
					cout << "Unknown command '" << userInput << "'." << endl;

					cout << "Please enter selection: ";
					getline( cin, userInput );

					if( userInput == "altF4" )
					{
						return;
					}
				}

				sp_model->trigger( userInput );
			}
		}

		void CLIDisplay::shutdown( )
		{
			running = false;
		}
		
	}
}
