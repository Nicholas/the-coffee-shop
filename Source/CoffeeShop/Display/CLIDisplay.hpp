/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : CLIDisplay.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


# ifndef _CLIDISPLAY_H
	# define _CLIDISPLAY_H

	# include "Observer.hpp"
	# include "DisplayModel.hpp"

	namespace CoffeeShop
	{
		namespace Display
		{
			class CLIDisplay : public Observer
			{
				private:
					bool running;
					DisplayModelSP sp_model;

				public:
					CLIDisplay( const DisplayModelSP & );
					virtual ~CLIDisplay( );

					virtual void update( );

					void display( );
					void shutdown( );
			};
		}
	}

# endif  //_CLIDISPLAY_H
