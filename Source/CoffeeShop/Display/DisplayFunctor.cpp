/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : DisplayFunctor.cpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


# include "DisplayFunctor.hpp"

namespace CoffeeShop
{
	namespace Display
	{
		/*
		DisplayFunctor::DisplayFunctor( const string & text_, const CallBackSP & sp_callBack_ )
		: text( text_ ),
		  sp_callBack( sp_callBack_ )
		{ }

		DisplayFunctor::DisplayFunctor( const DisplayFunctor & original )
		: text( original.text ),
		  sp_callBack( original.sp_callBack )
		{ }
		*/

		string DisplayFunctor::getText( ) const
		{
			return text;
		}
		
		/*
		void DisplayFunctor::operator( )( )
		{
			( * sp_callBack )( );
		}
		*/
	}
}
