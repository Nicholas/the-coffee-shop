/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : DisplayModel.cpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#include "DisplayModel.hpp"

namespace CoffeeShop
{
	namespace Display
	{
		DisplayModel::DisplayModel( )
		: Subject( ),
		  text( "" ),
		  sp_functions( new FunctionMap( ) ),
		  keys( 0 )
		{

		}

		DisplayModel::~DisplayModel( )
		{

		}

		string DisplayModel::getText( )
		{
			return text;
		}

		string DisplayModel::getText( string & key )
		{
			typename FunctionMap::const_iterator i = sp_functions->find( key );

			if( i == sp_functions->end( ) )
			{
				throw Exception( key );
			}

			return i->second->getText( );
		}
		
		void DisplayModel::setText( const string & text_ )
		{
			text = text_;
			notify( );
		}
		
		void DisplayModel::setFunctions( const FunctionMapSP & sp_functions_ )
		{
			sp_functions = sp_functions_;
			notify( );
		}
		
		void DisplayModel::setValue( const FunctionMapSP & sp_functions_, const string & text_ )
		{
			sp_functions = sp_functions_;
			text = text_;
			notify( );
		}

		Array< string > & DisplayModel::getKeys( )
		{
			return keys;
		}
		
		bool DisplayModel::contains( string & key )
		{
			return sp_functions->find( key ) != sp_functions->end( );
		}

		void DisplayModel::trigger( string & key )
		{
			typename FunctionMap::const_iterator i = sp_functions->find( key );

			if( i == sp_functions->end( ) )
			{
				throw Exception( key );
			}

			i->second->operator( )( );
		}

		void DisplayModel::notify( )
		{
			Array< string > newKeys( sp_functions->size( ) );

			int i = 0;
			FunctionMap::iterator iter;

			for( iter = sp_functions->begin( ); iter != sp_functions->end( ); iter++, i++ )
			{
				newKeys[ i ] = iter->first;
			}

			keys = newKeys;

			Subject::notify( );
		}
		
	}
}
