/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : DisplayModel.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


# ifndef _DISPLAYMODEL_H
	# define _DISPLAYMODEL_H

	# include "../../Configuration.hpp"
	# include "Subject.hpp"
	# include "DisplayFunctor.hpp"

	namespace CoffeeShop
	{
		namespace Display
		{
			class DisplayModel;
			typedef shared_ptr< DisplayModel > DisplayModelSP;

			class DisplayModel : public Subject
			{
				public:
					typedef map< string, DisplayFunctorSP > FunctionMap;
					typedef shared_ptr< FunctionMap > FunctionMapSP;

				private:
					string text;
					FunctionMapSP sp_functions;

					// Cashed Values
					Array< string > keys;

				public:
					DisplayModel( );
					virtual ~DisplayModel( );

					string getText( );
					string getText( string & );

					void setText( const string & );
					void setFunctions( const FunctionMapSP & );

					void setValue( const FunctionMapSP &, const string & );

					Array< string > & getKeys( );
					bool contains( string & );
					void trigger( string & );

					void notify( );

				public:
					class Exception: public std::exception
					{
						private:
							string unknownID;

						public:
							Exception( const string & unknownID_ )
								: unknownID( unknownID_ )
							{ }

							virtual const char * what( )
							{
								return "Unknown Display Functor Identifier";
							}

							const string & getIdentifier( )
							{
								return unknownID;
							}
					};
			};
		}
	}

# endif  //_DISPLAYMODEL_H
