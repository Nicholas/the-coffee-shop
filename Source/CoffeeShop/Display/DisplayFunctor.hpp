/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : DisplayFunctor.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


# ifndef _DISPLAYFUNCTOR_H
	# define _DISPLAYFUNCTOR_H

	# include "../../Configuration.hpp"

	namespace CoffeeShop
	{
		namespace Display
		{
			class DisplayFunctor;
			typedef shared_ptr< DisplayFunctor > DisplayFunctorSP;

			class DisplayFunctor : public Functor< void >
			{
				//public:
				//	typedef Functor< void > CallBack;
				//	typedef shared_ptr< CallBack > CallBackSP;

				private:
					string text;
				//	CallBackSP sp_callBack;

					DisplayFunctor( );

				public:
					DisplayFunctor( const DisplayFunctor & original )
					: Functor( original )
					{ }

				public:
				//	DisplayFunctor( const string &, const CallBackSP & );
				//	DisplayFunctor( const DisplayFunctor & );

					template< class FunctorType >
					DisplayFunctor( const string & text_, const FunctorType & functor )
					: Functor( functor ),
					  text( text_ )
					{ }

					template< class PrtObj, class MemFn >
					DisplayFunctor( const string & text_, const PrtObj & p, const MemFn & memFn )
					: Functor( p, memFn ),
					  text( text_ )
					{ }

					virtual ~DisplayFunctor( ) { }

					string getText( ) const;

				//	void operator( )( );
			};
		}
	}

#endif  //_DISPLAYFUNCTOR_H
