/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : Subject.cpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#include "Subject.hpp"
#include "Observer.hpp"

namespace CoffeeShop
{
	namespace Display
	{
		Subject::Subject( )
		: observers( )
		{

		}

		Subject::~Subject( )
		{

		}

		void Subject::attach( Observer * o )
		{
			observers.push_back( o );
		}
		
		void Subject::detach( Observer * o )
		{
			observers.remove( o );
		}
		
		void Subject::notify( )
		{
			list< Observer * >::iterator iter;

			for( iter = observers.begin( ); iter != observers.end( ); iter++ )
			{
				( * iter )->update( );
			}
		}
		
	}
}
