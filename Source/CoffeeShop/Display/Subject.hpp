/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : Subject.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


# ifndef _SUBJECT_H
	# define _SUBJECT_H

	# include "../../Configuration.hpp"
	# include "Observer.hpp"

	namespace CoffeeShop
	{
		namespace Display
		{
			class Subject
			{
				private:
					list< Observer * > observers;

				public:
					Subject( );
					virtual ~Subject( );

					virtual void attach( Observer * );
					virtual void detach( Observer * );
					virtual void notify( );
			};
		}
	}

#endif  //_SUBJECT_H
