/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : ProductHistory.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_PRODUCTHISTORY_H)
#define _PRODUCTHISTORY_H


namespace CoffeeShop
{
	namespace Services
	{
		template <class TillID> class ProductHistory
		{
		public:
			Product[] getTillHistory(TillID tillID);
		};
	}
}

#endif  //_PRODUCTHISTORY_H
