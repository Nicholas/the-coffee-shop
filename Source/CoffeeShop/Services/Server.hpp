/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : Server.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_SERVER_H)
#define _SERVER_H

#include "CoffeeMachineFactory.hpp"

namespace CoffeeShop
{
	namespace Services
	{
		template <class TillID> class Server
		{
		public:
			virtual CoffeeMachineFactory buildCoffeeMachineFactory(TillID id) = 0;
			virtual Product[] getHistory(TillID id) = 0;
			virtual void buyProduct(TillID id, Product * product) = 0;
		};
	}
}

#endif  //_SERVER_H
