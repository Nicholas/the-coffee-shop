/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : CoffeeShopServer.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_COFFEESHOPSERVER_H)
#define _COFFEESHOPSERVER_H

#include "Server.hpp"
#include "CoffeeMachineFactory.hpp"
#include "ProductStore.hpp"

namespace CoffeeShop
{
	namespace Services
	{
		class CoffeeShopServer : public Server
		{
			public:
				CoffeeMachineFactory buildCoffeeMachineFactory(TillID id);
				Product[] getHistory(TillID id);
				void buyProduct(TillID id, Product * product);
				CoffeeMachineFactory buildCoffeeMachineFactory(TillID id);
				Product[] getHistory(TillID id);
				void buyProduct(TillID id, Product * product);
			private:
				ProductStore productStore;
				ProductHistory< TillID > productHistory;
		};
	}
}

#endif  //_COFFEESHOPSERVER_H
