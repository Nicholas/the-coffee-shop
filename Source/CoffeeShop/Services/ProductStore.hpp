/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : ProductStore.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_PRODUCTSTORE_H)
#define _PRODUCTSTORE_H


namespace CoffeeShop
{
	namespace Services
	{
		class ProductStore
		{
		public:
			Condement[] getCondements();
			Coffee[] getCoffees();
		};
	}
}

#endif  //_PRODUCTSTORE_H
