/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : ServerProxy.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_SERVERPROXY_H)
#define _SERVERPROXY_H

#include "Server.hpp"
#include "CoffeeMachineFactory.hpp"
#include "CoffeeShopServer.hpp"

namespace CoffeeShop
{
	namespace Services
	{
		class ServerProxy : public Server
		{
		public:
			CoffeeMachineFactory buildCoffeeMachineFactory(TillID id);
			Product[] getHistory(TillID id);
			void buyProduct(TillID id, Product * product);
			CoffeeShopServer *RealSubject;
			CoffeeMachineFactory buildCoffeeMachineFactory(TillID id);
			Product[] getHistory(TillID id);
			void buyProduct(TillID id, Product * product);
		};
	}
}

#endif  //_SERVERPROXY_H
