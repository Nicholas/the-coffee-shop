/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : Controller.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_CONTROLLER_H)
#define _CONTROLLER_H

#include "DisplayModel.hpp"

namespace CoffeeShop
{
	namespace Controllers
	{
		class Controller
		{
		public:
			virtual void getText(string displayText) = 0;
			void registerFunction(string key, int displayText, void * ( * )( ) functor);
			void display();
		private:
			DisplayModel displayModel;
			map< string, DisplayFunctor > functions;
		};
	}
}

#endif  //_CONTROLLER_H
