/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : CondementSelection.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_CONDEMENTSELECTION_H)
#define _CONDEMENTSELECTION_H

#include "Controller.hpp"
#include "CoffeeMachine.hpp"

namespace CoffeeShop
{
	namespace Controllers
	{
		class CondementSelection : public Controller
		{
		public:
			void getText(string displayText);
			void selectCondement(string id);
			void removeConement(string id);
			void back();
			void cancel();
			void accept();
			void display(int CoffeeMachine *);
			CoffeeMachine *builder;
			void getText(string displayText);
		private:
			CoffeeMachine * coffeeMachine;
			void ( * )( ) backCallBack;
			void ( * )( ) cancelCallBack;
			void ( * )( Coffee * ) acceptCallBack;
		};
	}
}

#endif  //_CONDEMENTSELECTION_H
