/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : CoffeeSelection.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_COFFEESELECTION_H)
#define _COFFEESELECTION_H

#include "Controller.hpp"
#include "CoffeeMachineFactory.hpp"

namespace CoffeeShop
{
	namespace Controllers
	{
		class CoffeeSelection : public Controller
		{
		public:
			void getText(string displayText);
			void selectCoffee(string id);
			void cancel();
			CoffeeMachineFactory *factory;
			void getText(string displayText);
		private:
			CoffeeMachineFactory * coffeeMachineFactory;
			void ( * )( CoffeeMachine * ) selectCallBack;
			void ( * )( ) cancelCallBack;
		};
	}
}

#endif  //_COFFEESELECTION_H
