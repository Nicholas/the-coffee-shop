/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : History.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_HISTORY_H)
#define _HISTORY_H

#include "Controller.hpp"

namespace CoffeeShop
{
	namespace Controllers
	{
		class History : public Controller
		{
		public:
			Product[] ( * )( ) getProductsFunction;
			void getText(string displayText);
			void back();
			void getText(string displayText);
		private:
			void ( * )( ) backCallback;
		};
	}
}

#endif  //_HISTORY_H
