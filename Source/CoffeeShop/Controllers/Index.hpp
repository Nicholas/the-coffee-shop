/**
 *  A Little Coffee Shop
 *
 *  @ Project : CoffeeShop
 *  @ File Name : Index.hpp
 *  @ Date : 23/11/2013
 *  @ Author : Nicholas Welters
 */


#if !defined(_INDEX_H)
#define _INDEX_H

#include "Controller.hpp"

namespace CoffeeShop
{
	namespace Controllers
	{
		class Index : public Controller
		{
		public:
			void getText(string displayText);
			void brewCoffee();
			void veiwHistory();
			void getText(string displayText);
		private:
			void ( * )( ) brewCoffeeCallBack;
			void ( * )( ) viewHistoryCallBack;
		};
	}
}

#endif  //_INDEX_H
