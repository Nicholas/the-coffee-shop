/*
 * File:   Coniguration.hpp
 * Author: Nicholas Welters
 *
 * Created on 28 October 2013
 */

# ifndef _CONIGURATION_HPP_
	# define _CONIGURATION_HPP_

	//
	# include <list>
	# include <vector>

	namespace CoffeeShop
	{
		using std::list;
		using std::vector;
	}

	///////////////////////
	// My Tool Includes //
	/////////////////////
	# include "Tools/Common/Includes.hpp"
	# include "Tools/Common/Macros.hpp"
	# include "Tools/Array/Array.hpp"
	# include "Tools/Functors/Functor.hpp"
	# include "Tools/Factory/Factory.hpp"
	# include "Tools/Logger/Logger.hpp"

	# define UNUSED( var ) ( ( void ) var )

	namespace CoffeeShop
	{
		using std::cin;
		using std::shared_ptr;

		using namespace Tools;

		typedef Log< string, CoutLogger > InfoLogger;
		typedef Log< string, CerrLogger > ErrorLogger;
	}


#endif	/* CONIGURATION_HPP */
