/*
 * Main.cpp
 *
 *  Created on: 27 Oct 2013
 *      Author: Nicholas Welters
 */


#ifdef _TEST_MODE_
	#include "xTests/Test.hpp"
#else
	# include "Configuration.hpp"
#endif

int main( int argc, char **argv )
{
	try
	{
		#ifdef _TEST_MODE_
			Tests::Test test( &argc, argv );
			return test.run( );
		#else
			std::cout << "-----------\nCoffee Shop\n-----------\n" << std::flush;

			// TODO: Main Application entry point
			unused( argc );
			unused( argv );

			CoffeeShop::InfoLogger logger;
			logger.log( "System INIT" );

			return 0;
		#endif
	}
	catch( std::exception & e )
	{
		std::cout << "Unhandled exception: " << e.what( ) << std::flush;
	}
	catch( ... )
	{
		std::cout << "Unhandled exception of unknown type occurred!" << std::flush;
	}

	return 666;
}


