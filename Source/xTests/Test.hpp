/*
 * Test.hpp
 *
 *  Created on: 27 Oct 2013
 *      Author: Nicholas Welters
 */

#ifndef TEST_HPP_
#define TEST_HPP_

#include <gtest/gtest.h>

//#include "Tools/Array/Array1DTest.hpp"

namespace Tests
{
	class Test
	{
		public:
			Test( int * argc, char **argv );
			virtual ~Test( );

			int run( );
	};
} /* namespace Tests */
#endif /* TEST_HPP_ */
