/*
 * Test.cpp
 *
 *  Created on: 27 Oct 2013
 *      Author: Nicholas Welters
 */

#include "Test.hpp"

namespace Tests
{
	Test::Test( int * argc, char **argv )
	{
		testing::InitGoogleTest( argc, argv );
	}

	Test::~Test( ) { }

	int Test::run( )
	{
		return RUN_ALL_TESTS( );
	}
} /* namespace Tests */
