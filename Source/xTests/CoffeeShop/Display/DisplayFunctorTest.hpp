/*
 * DisplayFunctorTest.hpp
 *
 *  Created on: 4 Dec 2013
 *      Author: Nicholas Welters
 */

# ifndef DISPLAYFUNCTORTEST_HPP_
	# define DISPLAYFUNCTORTEST_HPP_

	# include <gtest/gtest.h>
	# include <string>

	# include "../../../CoffeeShop/Display/DisplayFunctor.hpp"

	using std::string;
	using CoffeeShop::Display::DisplayFunctor;

	class DisplayFunctorTest : public testing::Test
	{
		protected:
			DisplayFunctorTest( ) { }
			virtual ~DisplayFunctorTest( ) { }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};

# endif /* DISPLAYFUNCTORTEST_HPP_ */
