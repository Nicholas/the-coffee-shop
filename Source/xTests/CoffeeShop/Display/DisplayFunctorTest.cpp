/*
 * DisplayFunctorTest.cpp
 *
 *  Created on: 4 Dec 2013
 *      Author: Nicholas Welters
 */

# include "DisplayFunctorTest.hpp"

class TestDisplayFunctor
{
	public:
		int counter = 0;

		void count( )
		{
			counter++;
		}
};

TEST( DisplayFunctorTest, ShouldConstruct )
{
	TestDisplayFunctor functor;
	DisplayFunctor function( "Test", &functor, &TestDisplayFunctor::count );

	ASSERT_TRUE( true );
}

TEST( DisplayFunctorTest, HoldsExpectedObjects )
{
	std::string expected = "Test";
	TestDisplayFunctor functor;
	DisplayFunctor function( expected, &functor, &TestDisplayFunctor::count );

	function( );
	//functor( );

	ASSERT_EQ( 1, functor.counter );
	ASSERT_EQ( expected, function.getText( ) );
}
