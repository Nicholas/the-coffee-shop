/*
 * DisplayModelTest.hpp
 *
 *  Created on: 5 Dec 2013
 *      Author: Nicholas Welters
 */

# ifndef DISPLAYMODELTEST_HPP_
	# define DISPLAYMODELTEST_HPP_

	# include <gtest/gtest.h>

	# include "../../../CoffeeShop/Display/DisplayModel.hpp"

	using CoffeeShop::Display::DisplayModel;
	using CoffeeShop::Display::DisplayFunctor;
	using CoffeeShop::Display::DisplayFunctorSP;
	using CoffeeShop::Display::Observer;
	using std::string;

	class DisplayModelTest : public testing::Test
	{
		protected:
			DisplayModelTest( ) { }
			virtual ~DisplayModelTest( ) { }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};

#endif /* DISPLAYMODELTEST_HPP_ */
