/*
 * CLIDisplayTest.cpp
 *
 *  Created on: 5 Dec 2013
 *      Author: Nicholas Welters
 */

# include "CLIDisplayTest.hpp"

TEST( CLIDisplayTest, ShouldConstruct )
{
	CLIDisplay display( DisplayModelSP( nullptr ) );

	ASSERT_TRUE( true );
}

TEST( CLIDisplayTest, DisplaysModelAndAcceptsInput )
{
	stringstream input;
	ostringstream output;

	std::streambuf * cinbuf = std::cin.rdbuf( );
	std::streambuf * coutbuf = std::cout.rdbuf( );

	std::cin.rdbuf( input.rdbuf( ) );
	std::cout.rdbuf( output.rdbuf( ) );

	DisplayModelSP sp_model( new DisplayModel( ) );
	CLIDisplay display( sp_model );

	DisplayFunctorSP exit( new DisplayFunctor( "Quit on the Coffee Shop.", &display, &CLIDisplay::shutdown ) );

	DisplayModel::FunctionMapSP sp_functions( new DisplayModel::FunctionMap( ) );
	sp_functions->insert( typename DisplayModel::FunctionMap::value_type( "Q", exit ) );

	sp_model->setValue( sp_functions, "Test Display" );

	input << "wrong" << std::endl;
	input << "Q" << std::endl;

	display.display( );

	std::string expected;
	expected  = "Test Display\n";
	expected += "\n";
	expected += "|     |::|\n";
	expected += "|> Q <|::|> Quit on the Coffee Shop.\n";
	expected += "|     |::|\n";
	expected += "\n";
	expected += "Please enter selection: ";
	expected += "Unknown command 'wrong'.\n";
	expected += "Please enter selection: ";

	std::cin.rdbuf( cinbuf );
	std::cout.rdbuf( coutbuf );

	ASSERT_EQ( expected, output.str( ) );
}







