/*
 * CLIDisplayTest.hpp
 *
 *  Created on: 5 Dec 2013
 *      Author: Nicholas Welters
 */

# ifndef CLIDISPLAYTEST_HPP_
	# define CLIDISPLAYTEST_HPP_

	# include <gtest/gtest.h>

	# include "../../../CoffeeShop/Display/CLIDisplay.hpp"

	using CoffeeShop::Display::DisplayFunctor;
	using CoffeeShop::Display::DisplayFunctorSP;
	using CoffeeShop::Display::DisplayModel;
	using CoffeeShop::Display::DisplayModelSP;
	using CoffeeShop::Display::CLIDisplay;

	using std::stringstream;
	using std::ostringstream;

	class CLIDisplayTest: public testing::Test
	{
		public:
			CLIDisplayTest( ) { }
			virtual ~CLIDisplayTest( ) { }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};

# endif /* CLIDISPLAYTEST_HPP_ */
