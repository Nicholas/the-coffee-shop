/*
 * SubjectTest.hpp
 * _______________
 *
 *  Created on: 3 Dec 2013
 *      Author: Nicholas Welters
 */

# ifndef SUBJECTTEST_HPP_
	# define SUBJECTTEST_HPP_

	# include <gtest/gtest.h>

	# include "../../../CoffeeShop/Display/Subject.hpp"
	# include "../../../CoffeeShop/Display/Observer.hpp"

	using CoffeeShop::Display::Subject;
	using CoffeeShop::Display::Observer;

	class SubjectTest : public testing::Test
	{
		protected:
			SubjectTest( ){ }
			virtual ~SubjectTest( ){ }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};

# endif /* SUBJECTTEST_HPP_ */
