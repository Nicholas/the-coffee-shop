/*
 * DisplayModelTest.cpp
 *
 *  Created on: 5 Dec 2013
 *      Author: Nicholas Welters
 */

# include "DisplayModelTest.hpp"

struct testDisplayModelFunctor
{
	int counter = 0;

	void operator( )( )
	{
		counter++;
	}
};

TEST( DisplayModelTest, ShouldConstruct )
{
	DisplayModel model;

	ASSERT_TRUE( true );
}

TEST( DisplayModelTest, holdsExpected )
{
	DisplayModel model;

	string key = "key";
	string expected = "Display text.";
	testDisplayModelFunctor testFunctor;

	DisplayModel::FunctionMapSP sp_functions( new DisplayModel::FunctionMap( ) );
	sp_functions->insert( DisplayModel::FunctionMap::value_type( key, DisplayFunctorSP( new DisplayFunctor( expected, &testFunctor, &testDisplayModelFunctor::operator( ) ) ) ) );

	model.setText( expected );
	model.setFunctions( sp_functions );

	ASSERT_EQ( expected, model.getText( ) );

	/*
	typename DisplayModel::FunctionMap::const_iterator i = sp_functions->find( key );

	if( i != sp_functions->end( ) )
	{
		ASSERT_EQ( 0, testFunctor.counter );
	}
	else
	{
		std::cout << "The key was not found!" << std::endl;
		ADD_FAILURE( );
	}
	*/

	try
	{
		ASSERT_TRUE( model.contains( key ) );

		model.trigger( key );

		ASSERT_EQ( 1, testFunctor.counter );
		ASSERT_EQ( key, model.getKeys( )[ 0 ] );
		ASSERT_EQ( expected, model.getText( key ) );
	}
	catch( DisplayModel::Exception & e )
	{
		std::cout << e.what( ) << " - '" << e.getIdentifier( ) << "'" << std::endl;
		ADD_FAILURE( );
	}
}

class TestDisplayModelObserver : public Observer
{
	public:
		int counter;

		TestDisplayModelObserver( )
		: counter( 0 )
		{

		}
		virtual ~TestDisplayModelObserver( ){ }

		virtual void update( )
		{
			counter++;
		}
};

TEST( DisplayModelTest, subjectNotifiesObserver )
{
	DisplayModel model;
	TestDisplayModelObserver observer;

	model.attach( & observer );
	ASSERT_EQ( 0, observer.counter );

	model.setText( "" );
	ASSERT_EQ( 1, observer.counter );

	model.setFunctions( DisplayModel::FunctionMapSP( new DisplayModel::FunctionMap( ) ) );
	ASSERT_EQ( 2, observer.counter );

	model.setValue( DisplayModel::FunctionMapSP( new DisplayModel::FunctionMap( ) ), "" );
	ASSERT_EQ( 3, observer.counter );
}










