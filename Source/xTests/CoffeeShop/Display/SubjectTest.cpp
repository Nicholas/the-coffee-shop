/*
 * SubjectTest.cpp
 *
 *  Created on: 3 Dec 2013
 *      Author: Nicholas Welters
 */

# include "SubjectTest.hpp"


TEST( SubjectTest, ShouldConstruct )
{
	Subject subject;

	ASSERT_TRUE( true );
}

class TestObserver : public Observer
{
	public:
		int counter;

		TestObserver( )
		: counter( 0 )
		{

		}
		virtual ~TestObserver( ){ }

		virtual void update( )
		{
			counter++;
		}
};

TEST( SubjectTest, ContainerFunction )
{
	Subject subject;
	TestObserver observer;

	subject.attach( & observer );

	ASSERT_EQ( 0, observer.counter );

	subject.notify( );
	ASSERT_EQ( 1, observer.counter );

	subject.notify( );
	ASSERT_EQ( 2, observer.counter );

	subject.detach( & observer );
}
