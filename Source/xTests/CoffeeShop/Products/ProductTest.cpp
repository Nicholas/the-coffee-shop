/*
 * ProductTest.cpp
 *
 *  Created on: 26 Nov 2013
 *      Author: Nicholas Welters
 */

# include "ProductTest.hpp"


TEST( ProductTest, ShouldConstruct )
{
	Product product( "Test", 2.55 );

	ASSERT_TRUE( true );
}

TEST( ProductTest, HoldsBasicData )
{
	std::string expecetName = "product";
	double expectedValue = 20.2;

	Product product( expecetName, expectedValue );

	ASSERT_EQ( expecetName, product.getName( ) );
	ASSERT_DOUBLE_EQ( expectedValue, product.getValue( ) );
}

TEST( ProductTest, CloneCreatesANewCopy )
{
	std::string expecetName = "product";
	double expectedValue = 20.2;

	Product product( expecetName, expectedValue );

	Product * p_clone = nullptr;
	p_clone = product.clone( );

	ASSERT_NE( nullptr    , p_clone );
	ASSERT_EQ( expecetName, p_clone->getName( ) );

	ASSERT_DOUBLE_EQ( expectedValue, p_clone->getValue( ) );

	delete p_clone;
	p_clone = nullptr;
}

TEST( ProductTest, ZeroSizedComposite )
{
	Product product( "Test", 2.55 );
	size_t expected = 0;

	ASSERT_EQ( expected, product.size( ) );
}

TEST( ProductTest, CompositeMethodsThrowException )
{
	Product product( "Test", 2.55 );

	ProductSP sp_null( nullptr );

	ASSERT_THROW( product.add( sp_null )   , std::logic_error );
	ASSERT_THROW( product.remove( sp_null ), std::logic_error );
	ASSERT_THROW( product.get( 91846 )     , std::logic_error );
}











