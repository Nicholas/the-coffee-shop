/*
 * CoffeeTest.cpp
 *
 *  Created on: 27 Nov 2013
 *      Author: Nicholas Welters
 */

# include "CoffeeTest.hpp"


TEST( CoffeeTest, ShouldConstruct )
{
	Coffee coffee( "Test", 2.55 );

	ASSERT_TRUE( true );
}

TEST( CoffeeTest, CloneCreatesANewCopy )
{
	std::string expecetName = "product";
	double expectedValue = 20.2;

	Coffee coffee( expecetName, expectedValue );

	Coffee * p_clone = nullptr;
	p_clone = coffee.clone( );

	ASSERT_NE( nullptr    , p_clone );
	ASSERT_EQ( expecetName, p_clone->getName( ) );

	ASSERT_DOUBLE_EQ( expectedValue, p_clone->getValue( ) );

	delete p_clone;
	p_clone = nullptr;
}
