/*
 * ProductComposteTest.cpp
 *
 *  Created on: 27 Nov 2013
 *      Author: Nicholas Welters
 */

# include "ProductCompositeTest.hpp"

TEST( ProductCompositeTest, ShouldConstruct )
{
	ProductComposite product( "Test", 2.55 );

	ASSERT_TRUE( true );
}

TEST( ProductCompositeTest, CloneCreatesANewCopy )
{
	std::string expecetName = "product";
	double expectedValue = 20;

	ProductComposite product( expecetName, expectedValue );

	ProductComposite * p_clone = nullptr;
	p_clone = product.clone( );

	Product * p_covariantClone = nullptr;
	p_covariantClone = product.clone( );


	ASSERT_NE( nullptr    , p_clone );
	ASSERT_EQ( expecetName, p_clone->getName( ) );
	ASSERT_DOUBLE_EQ( expectedValue, p_clone->getValue( ) );

	ASSERT_NE( nullptr    , p_covariantClone );
	ASSERT_EQ( expecetName, p_covariantClone->getName( ) );
	ASSERT_DOUBLE_EQ( expectedValue, p_covariantClone->getValue( ) );


	delete p_clone;
	p_clone = nullptr;

	delete p_covariantClone;
	p_covariantClone = nullptr;
}

TEST( ProductCompositeTest, HoldsAProduct )
{
	std::string expectedName( "Test { Test Child }" );
	size_t expectedSize = 1;
	double expectedValue = 5.00;

	ProductComposite product( "Test { Test Child }", 2.56 );

	ProductSP sp_product( new Product( "Test Child", 2.44 ) );

	product.add( sp_product );

	ASSERT_EQ( expectedSize, product.size( ) );

	ASSERT_EQ( sp_product, product.get( 0 ) );
	ASSERT_EQ( expectedName, product.getName( ) );
	ASSERT_DOUBLE_EQ( expectedValue, product.getValue( ) );
}
















