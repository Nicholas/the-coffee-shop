/*
 * CondementTest.cpp
 *
 *  Created on: 27 Nov 2013
 *      Author: Nicholas Welters
 */

# include "CondimentTest.hpp"


TEST( CondimentTest, ShouldConstruct )
{
	Condiment condiment( "Test", 2.55 );

	ASSERT_TRUE( true );
}

TEST( CondimentTest, CloneCreatesANewCopy )
{
	std::string expecetName = "product";
	double expectedValue = 20.2;

	Condiment condiment( expecetName, expectedValue );

	Condiment * p_clone = nullptr;
	p_clone = condiment.clone( );

	ASSERT_NE( nullptr    , p_clone );
	ASSERT_EQ( expecetName, p_clone->getName( ) );

	ASSERT_DOUBLE_EQ( expectedValue, p_clone->getValue( ) );

	delete p_clone;
	p_clone = nullptr;
}
