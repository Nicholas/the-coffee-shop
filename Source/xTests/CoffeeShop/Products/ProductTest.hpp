/*
 * ProductTest.hpp
 *
 *  Created on: 26 Nov 2013
 *      Author: Nicholas Welters
 */

# ifndef PRODUCTTEST_HPP_
	# define PRODUCTTEST_HPP_

	# include <gtest/gtest.h>

	# include "../../../CoffeeShop/Products/Product.hpp"

	using namespace CoffeeShop::Products;

	class ProductTest : public testing::Test
	{
		protected:
			ProductTest( ){ }
			virtual ~ProductTest( ) { }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};

#endif /* PRODUCTTEST_HPP_ */
