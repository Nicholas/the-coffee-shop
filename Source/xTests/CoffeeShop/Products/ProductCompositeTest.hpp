/*
 * ProductComposteTest.hpp
 *
 *  Created on: 27 Nov 2013
 *      Author: Nicholas Welters
 */

#ifndef PRODUCTCOMPOSTETEST_HPP_
	#define PRODUCTCOMPOSTETEST_HPP_

	# include <gtest/gtest.h>

	# include "../../../CoffeeShop/Products/ProductComposite.hpp"

	using CoffeeShop::Products::Product;
	using CoffeeShop::Products::ProductSP;
	using CoffeeShop::Products::ProductComposite;

	class ProductCompositeTest : public testing::Test
	{
		public:
			ProductCompositeTest( ) { }
			virtual ~ProductCompositeTest( ) { }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};

#endif /* PRODUCTCOMPOSTETEST_HPP_ */
