/*
 * CoffeeTest.hpp
 * ______________
 *
 *  Created on: 27 Nov 2013
 *      Author: Nicholas Welters
 */

# ifndef COFFEETEST_HPP_
	# define COFFEETEST_HPP_

	# include <gtest/gtest.h>

	# include "../../../CoffeeShop/Products/Coffee.hpp"

	using CoffeeShop::Products::Coffee;

	class CoffeeTest : public testing::Test
	{
		protected:
			CoffeeTest( ){ }
			virtual ~CoffeeTest( ) { }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};

#endif /* COFFEETEST_HPP_ */
