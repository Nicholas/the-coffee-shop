/*
 * CondementTest.hpp
 *
 *  Created on: 27 Nov 2013
 *      Author: Nicholas Welters
 */

# ifndef CONDEMENTTEST_HPP_
	# define CONDEMENTTEST_HPP_

	# include <gtest/gtest.h>

	# include "../../../CoffeeShop/Products/Condiment.hpp"

	using CoffeeShop::Products::Condiment;

	class CondimentTest : public testing::Test
	{
		protected:
			CondimentTest( ){ }
			virtual ~CondimentTest( ) { }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};

#endif /* CONDEMENTTEST_HPP_ */
