/*
 * CoffeeMachineTest.hpp
 *
 *  Created on: 6 Dec 2013
 *      Author: Nicholas Welters
 */

# ifndef COFFEEMACHINETEST_HPP_
	# define COFFEEMACHINETEST_HPP_

	# include <gtest/gtest.h>

	# include "../../../CoffeeShop/Products/Condiment.hpp"
	//# include "../../../CoffeeShop/Products/Coffee.hpp"
	# include "../../../CoffeeShop/Creators/CoffeeMachine.hpp"

	using Tools::Functor;
	using Tools::Factory;

	using CoffeeShop::Products::Product;
	using CoffeeShop::Products::ProductSP;
	using CoffeeShop::Products::Coffee;
	using CoffeeShop::Products::CoffeeSP;
	using CoffeeShop::Products::Condiment;

	using CoffeeShop::Creators::CoffeeMachine;

	class CoffeeMachineTest
	{
		public:
			CoffeeMachineTest( ) { }
			virtual ~CoffeeMachineTest( ) { }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};

# endif /* COFFEEMACHINETEST_HPP_ */
