/*
 * CoffeeMachineTest.cpp
 *
 *  Created on: 6 Dec 2013
 *      Author: Nicholas Welters
 */

# include "CoffeeMachineTest.hpp"

class TestCoffeePot
{
	public:
		CoffeeSP operator( )( )
		{
			return CoffeeSP( new Coffee( "Test Coffee", 100 ) );
		}
};

struct testCondimentFactory
{
	Product * operator( )( ) const
	{
		return new Condiment( "Test Condiment", 50 );
	}
};

TEST( CoffeeMachineTest, ShouldConstruct )
{
	TestCoffeePot coffeePot;
	CoffeeMachine::CoffeePotSP sp_coffeePot( new CoffeeMachine::CoffeePot( coffeePot ) );
	CoffeeMachine::CondimentStoreSP sp_condomentStore( new CoffeeMachine::CondimentStore( ) );

	CoffeeMachine coffeeMachine( sp_coffeePot, sp_condomentStore );

	ASSERT_TRUE( true );
}

TEST( CoffeeMachineTest, CreatesACupOfCoffee )
{
	TestCoffeePot coffeePot;
	CoffeeMachine::CoffeePotSP sp_coffeePot( new CoffeeMachine::CoffeePot( coffeePot ) );
	CoffeeMachine::CondimentStoreSP sp_condomentStore( new CoffeeMachine::CondimentStore( ) );

	CoffeeMachine coffeeMachine( sp_coffeePot, sp_condomentStore );

	coffeeMachine.brewCoffee( );

	ASSERT_DOUBLE_EQ( 100.00, coffeeMachine.getCurrentValue( ) );

	CoffeeSP sp_coffee( coffeeMachine.getCoffee( ) );

	if( sp_coffee )
	{
		ASSERT_EQ( std::string( "Test Coffee" ), sp_coffee->getName( ) );
		ASSERT_DOUBLE_EQ( 100.00, sp_coffee->getValue( ) );
	}
	else
	{
		ADD_FAILURE( );
	}
}

TEST( CoffeeMachineTest, CreatesACupOfCoffeeWithACondement )
{
	TestCoffeePot coffeePot;
	testCondimentFactory condimentFactory;
	CoffeeMachine::CoffeePotSP sp_coffeePot( new CoffeeMachine::CoffeePot( coffeePot ) );
	CoffeeMachine::CondimentStoreSP sp_condomentStore( new CoffeeMachine::CondimentStore( ) );

	CoffeeMachine::CondimentFactory condimentFactoryFunctor( condimentFactory );

	sp_condomentStore->registerProduct( "Test", condimentFactoryFunctor );

	CoffeeMachine coffeeMachine( sp_coffeePot, sp_condomentStore );

	coffeeMachine.brewCoffee( );
	coffeeMachine.addCondement( "Test" );

	CoffeeSP sp_coffee( coffeeMachine.getCoffee( ) );

	if( sp_coffee )
	{
		ASSERT_EQ( std::string( "Test Coffee" ), sp_coffee->getName( ) );
		ASSERT_DOUBLE_EQ( 150.00, sp_coffee->getValue( ) );
	}
	else
	{
		ADD_FAILURE( );
	}
}








