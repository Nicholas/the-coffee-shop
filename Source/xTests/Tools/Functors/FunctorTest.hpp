/*
 * FunctorTest.hpp
 *
 *  Created on: 3 Nov 2013
 *      Author: Nicholas Welters
 */

# ifndef FUNCTORTEST_HPP_
	# define FUNCTORTEST_HPP_

	# include <gtest/gtest.h>

	# include "../../../Tools/Functors/Functor.hpp"

	using namespace Tools;

	class FunctorTest : public testing::Test
	{
		protected:
			FunctorTest( ){ }
			virtual ~FunctorTest( ) { }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};

# endif /* FUNCTORTEST_HPP_ */
