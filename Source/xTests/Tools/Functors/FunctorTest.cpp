/*
 * FunctorTest.cpp
 *
 *  Created on: 3 Nov 2013
 *      Author: Nicholas Welters
 */

# include "FunctorTest.hpp"

struct testFunctor1
{
	bool operator( )( )
	{
		return true;
	}
};

struct testFunctor2
{
	bool operator( )( bool arg1 )
	{
		return arg1;
	}
};

struct testFunctor3
{
	bool operator( )( bool arg1, bool arg2 )
	{
		return arg1 && arg2;
	}
};

TEST( FunctorTest, constructsFunctorHandler )
{
	testFunctor1 functor1;
	Functor< bool > function1( functor1 );
	ASSERT_TRUE( function1( ) );

	testFunctor2 functor2;
	Functor< bool, bool > function2( functor2 );
	ASSERT_TRUE( function2( true ) );

	testFunctor3 functor3;
	Functor< bool, bool, bool > function3( functor3 );
	ASSERT_TRUE( function3( true, true ) );
	ASSERT_FALSE( function3( true, false ) );
}

namespace Tools
{
	bool testFunction1( )
	{
		return true;
	}

	bool testFunction2( bool arg1 )
	{
		return arg1;
	}

	bool testFunction3( bool arg1, bool arg2 )
	{
		return arg1 && arg2;
	}
}

TEST( FunctorTest, constructsFunctionHandler )
{
	typedef bool (*Fun1)( );
	Fun1 f1 = testFunction1;
	Functor< bool > function1( f1 );
	ASSERT_TRUE( function1( ) );

	typedef bool (*Fun2)( bool );
	Fun2 f2 = testFunction2;
	Functor< bool, bool > function2( f2 );
	ASSERT_TRUE( function2( true ) );

	typedef bool (*Fun3)( bool, bool );
	Fun3 f3 = testFunction3;
	Functor< bool, bool, bool > function3( f3 );
	ASSERT_TRUE( function3( true, true ) );
	ASSERT_FALSE( function3( true, false ) );
}

const char * TypeConvertionTestFunction( double, double )
{
	static const char buffer[] = "Hello World!";
	return buffer;
}

TEST( FunctionTest, argAndReturnTypeConvertion )
{
	typedef const char * (*Fun)( double, double );
	Fun f = TypeConvertionTestFunction;
	Functor< string, int, int > function( f );
	string expected = "World!";

	EXPECT_EQ( expected, function( 10, 10 ).substr( 6 ) );
}

class TestMemFunction
{
	public:
		bool isTrue( )
		{
			return true;
		}

		double isPassThrough( double arg )
		{
			return arg;
		}
};

TEST( FunctionTest, constructsMemberFunctionHandler )
{
	TestMemFunction functions;

	Functor< bool > truthTest( &functions, &TestMemFunction::isTrue );
	Functor< double, int > passThroughTest( &functions, &TestMemFunction::isPassThrough );

	ASSERT_TRUE( truthTest( ) );
	EXPECT_DOUBLE_EQ( 10, passThroughTest( 10 ) );
}

/** // TODO: To implement Later
TEST( FunctionTest, functorsHaveBoundValues )
{
	TestMemFunction functions;

	Functor< bool > truthTest( &functions, &TestMemFunction::isTrue );
	Functor< double, int > passThroughTest( &functions, &TestMemFunction::isPassThrough );
	//Functor< double > passThroughTest2( BinderFirst< Functor< double, int >, int >( passThroughTest, 10 ) );

	ASSERT_TRUE( truthTest( ) );
	EXPECT_DOUBLE_EQ( 10, passThroughTest( 10 ) );
	//EXPECT_DOUBLE_EQ( 10, passThroughTest2( ) );
}
//*/



















