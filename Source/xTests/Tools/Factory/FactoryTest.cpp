/*
 * FactoryTest.cpp
 *
 *  Created on: 5 Nov 2013
 *      Author: Nicholas Welters
 */

# include "FactoryTest.hpp"

struct TestFactoryThisReturn
{

	TestFactoryThisReturn * operator( )( ) const
	{
		return nullptr;
	}
};

TEST( FactoryTest, constructAFactoryWithFunctor )
{
	TestFactoryThisReturn test;
	//Functor< TestFactoryThisReturn * > testConstructor( test );
	Factory< TestFactoryThisReturn, int, TestFactoryThisReturn > factory;


	factory.registerProduct( 666, test );
	ASSERT_EQ( nullptr, factory.createProduct( 666 ) );
}

TEST( FactoryTest, defaultThowsAnException )
{
	Factory< TestFactoryThisReturn, int, TestFactoryThisReturn > factory;
	typedef Factory< TestFactoryThisReturn, int, TestFactoryThisReturn >::Exception Expected;

	ASSERT_THROW( factory.createProduct( 234 ), Expected );
}
