/*
 * FactoryTest.hpp
 *
 *  Created on: 5 Nov 2013
 *      Author: Nicholas Welters
 */

# ifndef FACTORYTEST_HPP_
	# define FACTORYTEST_HPP_

	# include <gtest/gtest.h>

	# include "../../../Tools/Factory/Factory.hpp"
	# include "../../../Tools/Functors/Functor.hpp"

	using namespace Tools;

	class FactoryTest : public testing::Test
	{
		protected:
			FactoryTest( ){ }
			virtual ~FactoryTest( ) { }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};

# endif /* FACTORYTEST_HPP_ */
