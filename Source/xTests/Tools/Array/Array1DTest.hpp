/*
 * Array1DTest.hpp
 *
 *  Created on: 28 Oct 2013
 *      Author: Nicholas Welters
 */

# ifndef ARRAY1DTEST_HPP_
	# define ARRAY1DTEST_HPP_

	# include <gtest/gtest.h>

	# include "../../../Tools/Array/Array1D.hpp"

	using namespace Tools;

	class Array1DTest : public testing::Test
	{
		protected:
			Array1DTest( ){ }
			virtual ~Array1DTest( ) { }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};
# endif /* ARRAY1DTEST_HPP_ */
