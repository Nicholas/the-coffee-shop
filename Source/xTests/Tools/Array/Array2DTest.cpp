/*
 * Array2DTest.cpp
 *
 *  Created on: 31 Oct 2013
 *      Author: Nicholas Welters
 */

# include "Array2DTest.hpp"


TEST( Array2DTest, ShouldConstructAnEmptyArray )
{
	Array2D< int > array( 0, 0 );
	size_t expected = 0;
	EXPECT_EQ( expected, array.size( ) );
}

TEST( Array2DTest, Holds9Numbers )
{
	Array2D< int > array( 3, 3 );

	int expected_1 = 1;
	int expected_2 = 2;
	int expected_3 = 3;

	int expected_4 = 4;
	int expected_5 = 5;
	int expected_6 = 6;

	int expected_7 = 7;
	int expected_8 = 8;
	int expected_9 = 9;

	array[ 0 ][ 0 ] = expected_1;
	array[ 0 ][ 1 ] = expected_2;
	array[ 0 ][ 2 ] = expected_3;

	array[ 1 ][ 0 ] = expected_4;
	array[ 1 ][ 1 ] = expected_5;
	array[ 1 ][ 2 ] = expected_6;

	array[ 2 ][ 0 ] = expected_7;
	array[ 2 ][ 1 ] = expected_8;
	array[ 2 ][ 2 ] = expected_9;

	EXPECT_EQ( expected_1, array[ 0 ][ 0 ] );
	EXPECT_EQ( expected_2, array[ 0 ][ 1 ] );
	EXPECT_EQ( expected_3, array[ 0 ][ 2 ] );

	EXPECT_EQ( expected_4, array[ 1 ][ 0 ] );
	EXPECT_EQ( expected_5, array[ 1 ][ 1 ] );
	EXPECT_EQ( expected_6, array[ 1 ][ 2 ] );

	EXPECT_EQ( expected_7, array[ 2 ][ 0 ] );
	EXPECT_EQ( expected_8, array[ 2 ][ 1 ] );
	EXPECT_EQ( expected_9, array[ 2 ][ 2 ] );
}

TEST( Array2DTest, wrapperConstructor )
{
	int * * p_p_int2DArray = new int*[ 3 ];

	for( int i = 0; i < 3; i++ )
	{
		p_p_int2DArray[ i ] = new int[ 3 ];

		for( int j = 0; j < 3; j++ )
		{
			p_p_int2DArray[ i ][ j ] = ( i + 1 ) * ( j + 1 );
		}
	}

	Array2D< int > array2DWrapper( p_p_int2DArray, 3, 3 );

	for( int i = 0; i < 3; i++ )
	{
		for( int j = 0; j < 3; j++ )
		{
			EXPECT_EQ( ( i + 1 ) * ( j + 1 ), array2DWrapper[ i ][ j ] );
		}
	}

	EXPECT_EQ( p_p_int2DArray, array2DWrapper.pointer( ) );
}

TEST( Array2DTest, gettersAndSetters )
{
	Array2D< int > array2D( 3, 3 );

	for( int i = 0; i < 3; i++ )
	{
		for( int j = 0; j < 3; j++ )
		{
			int x = ( i + 1 ) * ( j + 1 );
			array2D.get( i ).set( j, x );
		}
	}

	for( int i = 0; i < 3; i++ )
	{
		for( int j = 0; j < 3; j++ )
		{
			EXPECT_EQ( ( i + 1 ) * ( j + 1 ), array2D.get( i ).get( j ) );
			EXPECT_EQ( ( i + 1 ) * ( j + 1 ), array2D.get( i, j ) );
		}
	}
}

TEST( Array2DTest, assignmentOperator )
{
	Array2D< int > array2D( 3, 3 );

	for( int i = 0; i < 3; i++ )
	{
		for( int j = 0; j < 3; j++ )
		{
			int x = ( i + 1 ) * ( j + 1 );
			array2D.get( i ).set( j, x );
		}
	}

	Array2D< int > copyArray2D( 0, 0 );
	copyArray2D = array2D;


	for( int i = 0; i < 3; i++ )
	{
		for( int j = 0; j < 3; j++ )
		{
			EXPECT_EQ( ( i + 1 ) * ( j + 1 ), copyArray2D.get( i ).get( j ) );
		}
	}

	copyArray2D[ 1 ][ 1 ] = 69;

	EXPECT_EQ( ( 1 + 1 ) * ( 1 + 1 ), array2D[ 1 ][ 1 ] );
	EXPECT_EQ( 69, copyArray2D[ 1 ][ 1 ] );
}

TEST( Array2DTest, cloneConstructor )
{
	Array2D< int > array2D( 3, 3 );

	for( int i = 0; i < 3; i++ )
	{
		for( int j = 0; j < 3; j++ )
		{
			int x = ( i + 1 ) * ( j + 1 );
			array2D.get( i ).set( j, x );
		}
	}

	Array2D< int > copyArray2D( array2D );


	for( int i = 0; i < 3; i++ )
	{
		for( int j = 0; j < 3; j++ )
		{
			EXPECT_EQ( ( i + 1 ) * ( j + 1 ), copyArray2D.get( i ).get( j ) );
		}
	}

	copyArray2D[ 1 ][ 1 ] = 69;

	EXPECT_EQ( ( 1 + 1 ) * ( 1 + 1 ), array2D[ 1 ][ 1 ] );
	EXPECT_EQ( 69, copyArray2D[ 1 ][ 1 ] );
}












