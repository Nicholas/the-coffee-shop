/*
 * Array2DTest.hpp
 *
 *  Created on: 31 Oct 2013
 *      Author: Nicholas Welters
 */

# ifndef ARRAY2DTEST_HPP_
	# define ARRAY2DTEST_HPP_

	# include <gtest/gtest.h>

	# include "../../../Tools/Array/Array2D.hpp"

	using namespace Tools;

	class Array2DTest : public testing::Test
	{
		protected:
			Array2DTest( ){ }
			virtual ~Array2DTest( ) { }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};
#endif /* ARRAY2DTEST_HPP_ */
