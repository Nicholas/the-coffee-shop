/*
 * Array1DTest.cpp
 *
 *  Created on: 28 Oct 2013
 *      Author: Nicholas Welters
 */

# include "Array1DTest.hpp"

TEST( Array1DTest, ShouldConstructAnEmptyArray )
{
	Array1D< int > array( 0 );
	size_t expected = 0;
	EXPECT_EQ( expected, array.size( ) );
}

TEST( Array1DTest, ShouldHoldThreeStringsUsingTheArrayOperators )
{
	Array1D< std::string > array( 3 );

	size_t expected = 3;
	EXPECT_EQ( expected, array.size( ) );

	std::string a = "This is one";
	std::string b = "This is two";
	std::string c = "This is three";

	array[ 0 ] = a;
	array[ 1 ] = b;
	array[ 2 ] = c;

	EXPECT_EQ( a, array[ 0 ] );
	EXPECT_EQ( b, array[ 1 ] );
	EXPECT_EQ( c, array[ 2 ] );
}

TEST( Array1DTest, ShouldHoldThreeStringsUsingTheGettersAndSetters )
{
	Array1D< std::string > array( 3 );

	size_t expected = 3;
	EXPECT_EQ( expected, array.size( ) );

	std::string a = "This is one";
	std::string b = "This is two";
	std::string c = "This is three";

	array.set( 0, a );
	array.set( 1, b );
	array.set( 2, c );

	EXPECT_EQ( a, array.get( 0 ) );
	EXPECT_EQ( b, array.get( 1 ) );
	EXPECT_EQ( c, array.get( 2 ) );
}

TEST( Array1DTest, ShouldThrowAnException )
{
	size_t expected = 0;
	string expectedException = "Index out of bounds: Attempting to access index 1 in an array of length 0";

	Array1D< int > array( expected );
	EXPECT_EQ( expected, array.size( ) );

	ASSERT_ANY_THROW( array[ 1 ] );

	bool thrownException = false;

	try
	{
		array[ 1 ] = 1392486;
	}
	catch( out_of_range & e )
	{
		EXPECT_EQ( expectedException, e.what( ) );
		thrownException = true;
	}

	ASSERT_TRUE( thrownException );
}

TEST( Array1DTest, CopyConstructorCreatsANewArray )
{
	Array1D< std::string > array( 3 );

	size_t expected = 3;
	EXPECT_EQ( expected, array.size( ) );

	std::string a = "This is one";
	std::string b = "This is two";
	std::string c = "This is three";
	std::string d = "This is the different one";

	array.set( 0, a );
	array.set( 1, b );
	array.set( 2, c );

	Array1D< std::string > copyArray( array );

	copyArray.set( 1, d );

	EXPECT_EQ( a, array.get( 0 ) );
	EXPECT_EQ( b, array.get( 1 ) );
	EXPECT_EQ( c, array.get( 2 ) );

	EXPECT_EQ( a, copyArray.get( 0 ) );
	EXPECT_EQ( d, copyArray.get( 1 ) );
	EXPECT_EQ( c, copyArray.get( 2 ) );
}

TEST( Array1DTest, AssignmentOperatorCreatsANewArray )
{
	Array1D< std::string > array( 3 );

	size_t expected = 3;
	EXPECT_EQ( expected, array.size( ) );

	std::string a = "This is one";
	std::string b = "This is two";
	std::string c = "This is three";
	std::string d = "This is the different one";

	array.set( 0, a );
	array.set( 1, b );
	array.set( 2, c );

	Array1D< std::string > copyArray( 0 );

	copyArray = array;

	copyArray.set( 1, d );

	EXPECT_EQ( a, array.get( 0 ) );
	EXPECT_EQ( b, array.get( 1 ) );
	EXPECT_EQ( c, array.get( 2 ) );

	EXPECT_EQ( a, copyArray.get( 0 ) );
	EXPECT_EQ( d, copyArray.get( 1 ) );
	EXPECT_EQ( c, copyArray.get( 2 ) );
}


TEST( Array1DTest, WrapperConstructorLooksAfterTheArray )
{
	std::string a = "This is one";
	std::string b = "This is two";
	std::string c = "This is three";

	std::string * p_array = new std::string[ 3 ];

	p_array[ 0 ] = a;
	p_array[ 1 ] = b;
	p_array[ 2 ] = c;

	Array1D< std::string > wapperArray( p_array, 3 );

	size_t expected = 3;
	EXPECT_EQ( expected, wapperArray.size( ) );

	EXPECT_EQ( p_array, wapperArray.pointer( ) );
}


