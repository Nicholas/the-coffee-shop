/*
 * LogTest.hpp
 *
 *  Created on: 3 Nov 2013
 *      Author: nic
 */

# ifndef LOGTEST_HPP_
	# define LOGTEST_HPP_

	# include <gtest/gtest.h>

	# include "../../../Tools/Logger/Logger.hpp"
	# include "../../../Tools/Logger/LoggingPolicy.hpp"

	using namespace Tools;

	using std::streambuf;
	using std::ostringstream;

	class LoggerTest : public testing::Test
	{
		protected:
			LoggerTest( ){ }
			virtual ~LoggerTest( ) { }

			virtual void SetUp( ) { }
			virtual void TearDown( ) { }
	};

# endif /* LOGTEST_HPP_ */
