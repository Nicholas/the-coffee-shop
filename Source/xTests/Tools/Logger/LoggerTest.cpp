/*
 * LogTest.cpp
 *
 *  Created on: 3 Nov 2013
 *      Author: Nicholas Welters
 */

# include "LoggerTest.hpp"

TEST( LoggerTest, constructsALogger )
{
	Log< string, CerrLogger > cerrLogger;
	Log< string, CoutLogger > coutLogger;
}

TEST( LoggerTest, coutPolicyDirectsOutPutToCout )
{
	string expected = "TEST";
	Log< string, CoutLogger > coutLogger;

	streambuf * oldCoutStreamBuf = cout.rdbuf();
	ostringstream strCout;
	cout.rdbuf( strCout.rdbuf( ) );

	coutLogger.log( expected );

	cout.rdbuf( oldCoutStreamBuf );

	EXPECT_EQ( expected + "\n", strCout.str( ) );
}

TEST( LoggerTest, cerrPolicyDirectsOutPutToCerr )
{
	string expected = "TEST";
	Log< string, CerrLogger > cerrLogger;

	streambuf * oldCoutStreamBuf = cerr.rdbuf();
	ostringstream strCerr;
	cerr.rdbuf( strCerr.rdbuf( ) );

	cerrLogger.log( expected );

	cerr.rdbuf( oldCoutStreamBuf );

	EXPECT_EQ( expected + "\n", strCerr.str( ) );
}
