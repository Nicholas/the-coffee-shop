# Simulation
# A Coffee Shop Cash Register System.

The coffee shop serves a few kinds of coffee such as HouseBlend, DarkRoast, Expresso, Mocha and Decaf with some optional condiments like Milk, Sugar or Cream.

When a client places an order the description and total cost of the order is displayed on the cash register "lcd" screen.

At the end of the day the manager will be able to look up the  total and transaction history on each cash register.


Since relocating to the UK I won't be able to work on this project until I get my self a PC again.