#############################
# CoffeeShop MakeFile       #
# Author: Nicholas Welters  #
#############################
PROJECT = CoffeeShop

CXX = g++-4.7
SOURCEDIR = Source

#	   -lfmodex64 \
	   -lglut \
	   -lGLEW \
	   -lGL \

#####################
# Template Classes #
###################\
	Tools/Array/Array1D.cpp \
	Tools/Log/LogPolicy.cpp \
	Tools/Log/Logger.cpp \
	\
	Tools/Functors/Functor.cpp \
	Tools/Functors/FunctorImpl.cpp \
	Tools/Functors/FunctorHandler.cpp \
	Tools/Functors/MemFunHandler.cpp \
	
###############
# Debug Build #
###############
LINK_TARGET = $(PROJECT).exe

BUILDDIR = Binary

INCLUDES = \

CXXFLAGS =  \
	-std=c++11 \
	-O0 \
	-g3 \
	-pedantic \
	-Wfatal-errors \
	-Wall \
	-Wextra \
	-Werror \
	-Wstrict-null-sentinel \
	-Wfloat-equal \
	-Wshadow \
	-Wconversion \
	-Wunreachable-code \
	-static \
	-fmessage-length=0 \
	-DGL_GLEXT_PROTOTYPES

LIBS = \
	-L/usr/lib \
	-lpthread \
	-lX11 \
	-lXm \
	-lXt \
	   
SOURCES =  \
	./Main.cpp \
	\
	CoffeeShop/Products/Product.cpp \
	CoffeeShop/Products/Condiment.cpp \
	CoffeeShop/Products/ProductComposite.cpp \
	CoffeeShop/Products/Coffee.cpp \
	\
	CoffeeShop/Display/Subject.cpp \
	CoffeeShop/Display/DisplayFunctor.cpp \
	CoffeeShop/Display/DisplayModel.cpp \
	CoffeeShop/Display/CLIDisplay.cpp \
	\
	CoffeeShop/Creators/CoffeeMachine.cpp\
	
OBJECTS = $(SOURCES:%.cpp=$(BUILDDIR)/%.o)

all : $(LINK_TARGET)
	
$(LINK_TARGET) : $(OBJECTS)
	$(CXX) -o $@ $^ $(LIBS)
	
$(BUILDDIR)/%.o : $(SOURCEDIR)/%.cpp
	mkdir -p $(dir $@)
	$(CXX) $(INCLUDES) $(CXXFLAGS) -o $@ -c $<

###############
# Debug Build #
###############

##############
# Test Build #
##############
LINK_TARGET_TEST = $(PROJECT)Test.exe

BUILDDIR_TEST = $(BUILDDIR)Test
TEST_ROOT = xTests
	
CXXFLAGS_TEST = $(CXXFLAGS) \
				-D_TEST_MODE_ \

INCLUDES_TEST = $(INCLUDES) \
			-I./API/gtest-1.6.0/include
				
LIBS_TEST = $(LIBS) \
			-L./API/gtest-1.6.0/lib \
			-lgtest

SOURCES_TEST = $(SOURCES) \
	$(TEST_ROOT)/Test.cpp \
	$(TEST_ROOT)/Tools/Array/Array1DTest.cpp \
	$(TEST_ROOT)/Tools/Array/Array2DTest.cpp \
	$(TEST_ROOT)/Tools/Logger/LoggerTest.cpp \
	$(TEST_ROOT)/Tools/Functors/FunctorTest.cpp \
	$(TEST_ROOT)/Tools/Factory/FactoryTest.cpp \
	\
	$(TEST_ROOT)/CoffeeShop/Products/ProductTest.cpp \
	$(TEST_ROOT)/CoffeeShop/Products/CondimentTest.cpp \
	$(TEST_ROOT)/CoffeeShop/Products/ProductCompositeTest.cpp \
	$(TEST_ROOT)/CoffeeShop/Products/CoffeeTest.cpp \
	\
	$(TEST_ROOT)/CoffeeShop/Display/SubjectTest.cpp \
	$(TEST_ROOT)/CoffeeShop/Display/DisplayFunctorTest.cpp \
	$(TEST_ROOT)/CoffeeShop/Display/DisplayModelTest.cpp \
	$(TEST_ROOT)/CoffeeShop/Display/CLIDisplayTest.cpp \
	\
	$(TEST_ROOT)/CoffeeShop/Creators/CoffeeMachineTest.cpp\
	
OBJECTS_TEST = $(SOURCES_TEST:%.cpp=$(BUILDDIR_TEST)/%.o)

tests : $(LINK_TARGET_TEST)

$(LINK_TARGET_TEST) : $(OBJECTS_TEST)
	mkdir -p $(dir $@)
	$(CXX) -o $@ $^ $(LIBS_TEST)
	
$(BUILDDIR_TEST)/%.o : $(SOURCEDIR)/%.cpp
	mkdir -p $(dir $@)
	$(CXX) $(INCLUDES_TEST) $(CXXFLAGS_TEST) -o $@ -c $<
	
##############
# Test Build #
##############
	
################
# Binary Reset #
################
REBUILDABLES = $(OBJECTS) $(LINK_TARGET) $(OBJECTS_TEST) $(LINK_TARGET_TEST)

.PHONY: clean
	
clean : 
	rm -f $(REBUILDABLES)
################
# Binary Reset #
################




















# Release	
#CXXFLAGS =  -std=c++11 \
			-O2 \
			-pedantic \
			-Wall \
			-Wextra \
			-Wfatal-errors \
			-Werror \
			-Wstrict-null-sentinel \
			-Wfloat-equal \
			-Wshadow \
			-Wconversion \
			-Wunreachable-code \
			-static \
			-fmessage-length=0



#deps.mak:
#	$(CXX) -MM $(CXXFLAGS) *.cpp > deps.mak
#
#include deps.mak
	
